package ua.lsi.consuls.api.model;

import lombok.Data;
import ua.lsi.consuls.core.battle.RoundResults;

import java.util.List;
import java.util.stream.Collectors;

@Data
public class ApiRound {
    private String roundLabel;
    private List<ApiUnit> humanShips;
    private List<ApiUnit> reptileShips;
    public ApiRound(RoundResults rr) {
        roundLabel = rr.getRoundLabel();
        humanShips = rr.getHumanShips().stream()
                .map(ApiUnit::new)
                .collect(Collectors.toList());
        reptileShips = rr.getReptileShips().stream()
                .map(ApiUnit::new)
                .collect(Collectors.toList());
    }
}
