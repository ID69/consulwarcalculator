package ua.lsi.consuls.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ua.lsi.consuls.api.model.ApiUserConfig;
import ua.lsi.consuls.api.service.UserService;

@RestController
@RequestMapping("/user/{username}/config")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping
    public ResponseEntity saveUserConfig(@PathVariable String username, @RequestBody ApiUserConfig model) {
        userService.saveConfig(username, model);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping
    public ResponseEntity<ApiUserConfig> get(@PathVariable String username) {

        ApiUserConfig apiConfig = userService.getApiConfig(username);
        if (apiConfig == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(apiConfig, HttpStatus.OK);
    }
}
