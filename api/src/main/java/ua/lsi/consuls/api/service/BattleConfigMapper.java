package ua.lsi.consuls.api.service;

import com.google.common.base.Converter;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Component;
import ua.lsi.consuls.api.model.db.UserConfig;
import ua.lsi.consuls.core.battle.BattleConfig;
import ua.lsi.consuls.core.enums.flat.Avatar;
import ua.lsi.consuls.core.enums.flat.BonusItem;
import ua.lsi.consuls.core.enums.flat.Room;
import ua.lsi.consuls.core.enums.flat.Tron;
import ua.lsi.consuls.core.ships.SpaceUnit;
import ua.lsi.consuls.core.util.BonusItemsHelper;
import ua.lsi.consuls.core.util.NameToClassLinker;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class BattleConfigMapper extends Converter<UserConfig, BattleConfig> {

    @Override
    protected BattleConfig doForward(UserConfig userConfig) {
        BattleConfig bc = new BattleConfig();
        BeanUtils.copyProperties(userConfig, bc);
        bc.setUpdatesMap(transformToMapWithClasses(userConfig));
        bc.setBonusItems(getBonusItems(userConfig));
        return bc;
    }

    private Map<Class<? extends SpaceUnit>, Integer> transformToMapWithClasses(UserConfig userConfig) {
        if (userConfig.getUpdatesMap() == null) {
            return Collections.emptyMap();
        }
        return userConfig.getUpdatesMap().entrySet().stream()
                .collect(Collectors.toMap(
                        o -> NameToClassLinker.getInstance().getClassForName(o.getKey()),
                        Map.Entry::getValue));
    }

    private List<BonusItem> getBonusItems(UserConfig userConfig) {
        if (userConfig.getBonusItems() == null || userConfig.getBonusItems().isEmpty()) {
            return Collections.emptyList();
        }

        Map<String, BonusItem> items = BonusItemsHelper.getInstance().getAllBonusItems();

        return userConfig.getBonusItems().stream().map(items::get).collect(Collectors.toList());
    }

    @Override
    protected UserConfig doBackward(BattleConfig battleConfig) {
        throw new UnsupportedOperationException();
    }
}
