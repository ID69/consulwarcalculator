package ua.lsi.consuls.api.service;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ua.lsi.consuls.api.model.ApiUserConfig;
import ua.lsi.consuls.api.model.db.UserConfig;
import ua.lsi.consuls.api.repository.UserConfigurationRepository;
import ua.lsi.consuls.core.battle.BattleConfig;

import java.util.HashMap;
import java.util.Map;

@Component
public class UserService {
    @Autowired
    private UserConfigurationRepository userRepository;
    private static final Map<String, UserConfig> chachedConfigs = new HashMap<>();

    @Autowired
    private BattleConfigMapper bcMapper;

    public BattleConfig getConfig(String username){

        UserConfig uc = getUserConfigTryChache(username);

        return bcMapper.doForward(uc);
    }

    public ApiUserConfig getApiConfig(String username) {
        UserConfig uc = getUserConfigTryChache(username);
        if (uc == null) {
            return null;
        }
        ApiUserConfig apiUserConfig = new ApiUserConfig();
        BeanUtils.copyProperties(uc, apiUserConfig);
        return apiUserConfig;
    }

    public void saveConfig(String username, ApiUserConfig bc) {
        UserConfig oldUc = userRepository.findByUsername(username);
        if (oldUc == null) {
            oldUc = new UserConfig();
        }
        BeanUtils.copyProperties(bc, oldUc);
        oldUc.setUsername(username);

        userRepository.save(oldUc);
        chachedConfigs.put(username, oldUc);
    }


    private UserConfig getUserConfigTryChache(String username) {
        UserConfig uc = chachedConfigs.get(username);

        if (uc == null) {
            uc = userRepository.findByUsername(username);
        }
        return uc;
    }
}
