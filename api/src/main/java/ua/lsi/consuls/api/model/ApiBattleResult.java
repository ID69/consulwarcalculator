package ua.lsi.consuls.api.model;

import lombok.Data;
import ua.lsi.consuls.core.battle.BattleCalculations;
import ua.lsi.consuls.core.model.ResourceContainer;

import java.util.List;
import java.util.stream.Collectors;

@Data
public class ApiBattleResult {
    private String humanStatus;
    private String reptileStatus;
    private List<ApiRound> rounds;
    private ResourceContainer repair;
    private ResourceContainer reward;
    private ResourceContainer loses;
    private ResourceContainer profit;

    public ApiBattleResult(BattleCalculations bc) {
        humanStatus = bc.getHumanStatus();
        reptileStatus = bc.getReptileStatus();
        rounds = bc.getRounds().stream()
                .map(ApiRound::new)
                .collect(Collectors.toList());
        repair = bc.getRepair();
        reward = bc.getReward();
        loses = bc.getLoses();
        profit = bc.getProfit();
    }
}
