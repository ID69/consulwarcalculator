package ua.lsi.consuls.api.repository;

import org.socialsignin.spring.data.dynamodb.repository.EnableScan;
import org.springframework.data.repository.CrudRepository;
import ua.lsi.consuls.api.model.db.UserConfig;


@EnableScan
public interface UserConfigurationRepository extends CrudRepository<UserConfig, String> {

    UserConfig findByUsername(String username);
}
