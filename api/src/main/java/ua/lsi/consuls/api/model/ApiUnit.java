package ua.lsi.consuls.api.model;

import lombok.Data;
import ua.lsi.consuls.core.ships.UnitGroup;

@Data
public class ApiUnit {
    private String name;
    private String displayName;
    private Long count;
    private Long currentAttack;
    private Long currentHealth;

    public ApiUnit(UnitGroup ug) {
        name = ug.getSpaceUnit().getName();
        displayName = ug.getSpaceUnit().getDisplayName();
        currentAttack = ug.getCurrentAttack();
        currentHealth = ug.getCurrentHealth();
        count = ug.getCount();
    }
}
