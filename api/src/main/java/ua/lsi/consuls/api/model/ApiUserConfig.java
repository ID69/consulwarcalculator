package ua.lsi.consuls.api.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import ua.lsi.consuls.core.enums.flat.BonusItem;

import java.util.List;
import java.util.Map;

@Data
public class ApiUserConfig {
    private Map<String, Integer> updatesMap;
    //donate
    private Boolean crazyDonateBonus;
    //achievements
    private Boolean satanMinister;
    private Boolean pirateRaid;
    private Boolean braveCaptain;
    private Boolean headlessAdmiral;
    private Boolean lepreconKiller;
    private Boolean coldbloodedCalmly;
    private Boolean lightBringer;
    private Boolean levTolstoy;
    //research
    private Integer energyLvl;
    private Integer alloyLvl;
    private Integer scienceLvl;
    private Integer ikeaLvl;
    private Integer defenseEngineeringLvl;
    private Integer hyperdriveLvl;
    private Integer nanotechnologyLvl;
    private Integer plasmoidConverterLvl;
    private Integer doomDayCalibrationLvl;
    //building residential
    private Integer spacePortLvl;
    private Integer entertainmentCenterLvl;
    private Integer blackMarketLvl;
    //building military
    private Integer barracksLvl;
    private Integer militaryFactoryLvl;
    private Integer airfieldLvl;
    private Integer shipyyardLvl;
    private Integer defenseComplexLvl;
    private Integer gatesLvl;
    private Integer engineeringComplexLvl;
    private Integer oscdFabricLvl;
    //room
    @ApiModelProperty(example = "NO_BONUS,JOHN_SNOW.GAME_OF_THRONES,CRUEL.GAME_OF_THRONES")
    private List<String> bonusItems;
    //general damage
    private Double humanDamagePercent;
    private Double reptileDamagePercent;
    private Double targetChangeModifierCoefficient;
    //custom
    private Double attackUnitCustomPercent;
    private Double healthUnitCustomPercent;
    private Double attackDefenceCustomPercent;
    private Double healthDefenceCustomPercent;
}
