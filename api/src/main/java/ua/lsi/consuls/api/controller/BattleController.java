package ua.lsi.consuls.api.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.lsi.consuls.api.model.ApiBattleRequest;
import ua.lsi.consuls.api.model.ApiBattleResult;
import ua.lsi.consuls.api.service.UserService;
import ua.lsi.consuls.core.battle.BattleCalculations;
import ua.lsi.consuls.core.battle.BattleConfig;
import ua.lsi.consuls.core.enums.BattleType;
import ua.lsi.consuls.core.ships.SpaceUnit;
import ua.lsi.consuls.core.ships.UnitGroup;
import ua.lsi.consuls.core.util.NameToClassLinker;
import ua.lsi.consuls.core.util.ReptileFleetProvider;
import ua.lsi.consuls.core.util.SpaceUnitCreationHelper;

import java.util.Map;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/battle")
public class BattleController {

    @Autowired
    private UserService userService;

    @PostMapping
    public ApiBattleResult calculateBattle(@RequestBody ApiBattleRequest request) {
        BattleConfig battleConfig = userService.getConfig(request.getUsername());
        battleConfig.setAttackType(request.getAttackType());
        battleConfig.setComplexity(request.getComplexity());
        Map<Class<? extends SpaceUnit>, UnitGroup> reptileShips;
        Map<Class<? extends SpaceUnit>, UnitGroup> humanShips;
        if (request.getReptileShips() == null) {
            reptileShips = ReptileFleetProvider.getInstance().getFleetsByTypeAndLvl(battleConfig);
        } else {
            reptileShips = request.getReptileShips().entrySet().stream()
                    .map(e -> UnitGroup.create(SpaceUnitCreationHelper.getInstance()
                            .getInstanceOf(NameToClassLinker.getInstance().getClassForName(e.getKey()),
                                    battleConfig), e.getValue()))
                    .collect(Collectors.toMap(UnitGroup::getUnitClass, o -> o));
        }
        humanShips = request.getHumanShips().entrySet().stream()
                .map(e -> UnitGroup.create(SpaceUnitCreationHelper.getInstance()
                        .getInstanceOf(NameToClassLinker.getInstance().getClassForName(e.getKey()),
                                battleConfig), e.getValue()))
                .collect(Collectors.toMap(UnitGroup::getUnitClass, o -> o));

        BattleCalculations bc = new BattleCalculations(humanShips, reptileShips, battleConfig);

        bc.makeBattle(BattleType.STANDART);
        return new ApiBattleResult(bc);
    }
}
