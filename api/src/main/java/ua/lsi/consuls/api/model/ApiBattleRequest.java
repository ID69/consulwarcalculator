package ua.lsi.consuls.api.model;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import ua.lsi.consuls.core.enums.AttackType;
import ua.lsi.consuls.core.enums.Complexity;

import java.util.Map;

@Data
public class ApiBattleRequest {
    private Map<String, Integer> humanShips;
    private Map<String, Integer> reptileShips;
    private String username;
    @ApiModelProperty(example = "PATROL")
    private AttackType attackType;
    @ApiModelProperty(example = "ONE")
    private Complexity complexity;
}
