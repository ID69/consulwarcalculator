package ua.lsi.consuls.api.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static com.google.common.base.Predicates.not;
import static com.google.common.base.Predicates.or;
import static springfox.documentation.builders.PathSelectors.ant;

@EnableSwagger2
@Configuration
public class SwaggerConfig {

    @Bean
    public Docket globalApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(
                        not(
                                or(
                                        ant("/docsreload"),
                                        ant("/error")
                                )
                        )
                )
                .build()
                .useDefaultResponseMessages(false)
                .apiInfo(apiInfo());
    }



    protected ApiInfo apiInfo() {
        return new ApiInfoBuilder().title("ConsulWar Calculator API")
                .description("ConsulWar Calculator API")
                .version("v1")
                .build();
    }

}
