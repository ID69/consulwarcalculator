package ua.lsi.consuls.core.ships.reptile;

import ua.lsi.consuls.core.battle.BattleConfig;

/**
 * @author ID69
 */
public class Lacertian extends ReptileShip {
    public static final String NAME = "Ящер";

    public Lacertian(BattleConfig battleConfig) {
        super(battleConfig);
    }
}
