package ua.lsi.consuls.core.ships.human.defence;

import ua.lsi.consuls.core.battle.BattleConfig;

/**
 * @author ID69
 */
public class DoomsDayGun extends DefenceUnit {
    public static final String NAME = "ОСО";

    public DoomsDayGun(BattleConfig battleConfig) {
        super(battleConfig);
    }
}