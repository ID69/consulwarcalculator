package ua.lsi.consuls.core.ships.reptile;

import ua.lsi.consuls.core.battle.BattleConfig;

/**
 * @author ID69
 */
public class Armadillo extends ReptileShip {
    public static final String NAME = "Броненосец";

    public Armadillo(BattleConfig battleConfig) {
        super(battleConfig);
    }
}
