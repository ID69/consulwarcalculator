package ua.lsi.consuls.core.ships.human;

import ua.lsi.consuls.core.battle.BattleConfig;

/**
 * @author ID69
 */
public class Wasp extends LightShip {
    public static final String NAME = "Оса";

    public Wasp(BattleConfig battleConfig) {
        super(battleConfig);
    }
}
