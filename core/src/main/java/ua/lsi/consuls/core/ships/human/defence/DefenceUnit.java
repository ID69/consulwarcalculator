package ua.lsi.consuls.core.ships.human.defence;

import ua.lsi.consuls.core.battle.BattleConfig;
import ua.lsi.consuls.core.enums.UnitType;
import ua.lsi.consuls.core.ships.human.HumanShip;

/**
 * @author ID69
 */
public abstract class DefenceUnit extends HumanShip {
    public static final Float[] defenseComplexBonuses = {0f, 0.1f, 0.15f, 0.25f, 0.35f, 0.5f};

    public DefenceUnit(BattleConfig battleConfig) {
        super(battleConfig);
    }

    @Override
    public UnitType getType() {
        return UnitType.HUMAN_DEFENCE;
    }

    @Override
    public Integer getAttack() {
        int attack = getMinAttack() + Double.valueOf((getMaxAttack() - getMinAttack()) * battleConfig.getHumanDamagePercent()).intValue();
        return Math.round(attack + (int) (attack * battleConfig.getDefenceAttackPercentBonus()));
    }

    @Override
    public Integer getHealth() {
        int health = getBaseHealth();
        return Math.round(health + (int) (health * battleConfig.getDefenceHealthPercentBonus()));
    }

    @Override
    public Double getMetal() {
        return price.getMetal() - (int) (price.getMetal() * 0.003f * battleConfig.getDefenseComplexLvl());
    }

    @Override
    public Double getCrystal() {
        return price.getCrystal() - (int) (price.getCrystal() * 0.003f * battleConfig.getDefenseComplexLvl());
    }

    @Override
    public Integer getTime() {
        Integer firsLvl = price.getHumanTime() - (int) (price.getHumanTime() * defenseComplexBonuses[battleConfig.getDefenseComplexLvl() / 20]);
        Integer secondLvl = firsLvl;
        if (battleConfig.getCrazyDonateBonus()) {
            secondLvl = (int) (firsLvl - firsLvl * 0.25f);
        }
        return secondLvl;
    }
}
