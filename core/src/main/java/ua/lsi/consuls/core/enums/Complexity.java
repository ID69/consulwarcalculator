package ua.lsi.consuls.core.enums;

import lombok.Getter;

/**
 * @author ID69
 */
public enum Complexity {
    ONE(1),
    TWO(2),
    THREE(3),
    FOUR(4),
    FIVE(5),
    SIX(6),
    SEVEN(7);

    @Getter
    private int complexity;

    Complexity(int complexityVal) {
        complexity = complexityVal;
    }

    @Override
    public String toString() {
        return Integer.toString(complexity);
    }
}
