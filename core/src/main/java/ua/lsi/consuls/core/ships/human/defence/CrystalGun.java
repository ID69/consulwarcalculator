package ua.lsi.consuls.core.ships.human.defence;

import ua.lsi.consuls.core.battle.BattleConfig;

/**
 * @author ID69
 */
public class CrystalGun extends DefenceUnit {
    public static final String NAME = "Кристалл-Ган";

    public CrystalGun(BattleConfig battleConfig) {
        super(battleConfig);
    }
}