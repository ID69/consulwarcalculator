package ua.lsi.consuls.core.util;

import ua.lsi.consuls.core.enums.flat.Avatar;
import ua.lsi.consuls.core.enums.flat.BonusItem;
import ua.lsi.consuls.core.enums.flat.Room;
import ua.lsi.consuls.core.enums.flat.Tron;

import java.util.HashMap;
import java.util.Map;

public class BonusItemsHelper {
    private Map<String, BonusItem> ITEMS = new HashMap<>();


    private static class Holder {
        static BonusItemsHelper instance = new BonusItemsHelper();
    }

    public static BonusItemsHelper getInstance() {
        return BonusItemsHelper.Holder.instance;
    }

    private BonusItemsHelper() {
        for (Avatar item : Avatar.values()) {
            ITEMS.put(item.name(), item);
        }

        for (Room item : Room.values()) {
            ITEMS.put(item.name(), item);
        }

        for (Tron item : Tron.values()) {
            ITEMS.put(item.name(), item);
        }
    }

    public Map<String, BonusItem> getAllBonusItems() {
        return ITEMS;
    }
}
