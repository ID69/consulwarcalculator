package ua.lsi.consuls.core.util;

import ua.lsi.consuls.core.battle.BattleConfig;
import ua.lsi.consuls.core.enums.AttackType;
import ua.lsi.consuls.core.model.ResourceContainer;
import ua.lsi.consuls.core.model.dto.ReptileFleetDTO;
import ua.lsi.consuls.core.model.dto.UnitDTO;
import ua.lsi.consuls.core.ships.SpaceUnit;
import ua.lsi.consuls.core.ships.UnitGroup;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author ID69
 */
public class ReptileFleetProvider {

    private static class SingletonHolder {
        static final ReptileFleetProvider INSTANCE = new ReptileFleetProvider();
    }

    public static ReptileFleetProvider getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private Map<AttackType, Map<Integer, ReptileFleetDTO>> reptileFleetsMap = new HashMap<>();

    private ReptileFleetProvider() {
        load(AttackType.PATROL, "/fleets/ReptilePatrol.json");
        load(AttackType.DEFENCE, "/fleets/ReptileDefence.json");
        load(AttackType.BATTLE, "/fleets/ReptileBattle.json");
        load(AttackType.TRADE, "/fleets/ReptileTrade.json");
        load(AttackType.IAMREADY, "/fleets/ReptileIAmReady.json");
        load(AttackType.PRISON, "/fleets/ReptilePrison.json");
    }

    private void load(AttackType type, String fileName) {
        Map<Integer, ReptileFleetDTO> fleetsMap = reptileFleetsMap.getOrDefault(type, new LinkedHashMap<>());
        ReptileFleetDTO[] fleets = DataFileParser.getInstance().parse(fileName, ReptileFleetDTO[].class);
        Arrays.stream(fleets)
                .forEach(fleet -> fleetsMap.put(fleet.getLvl(), fleet));

        reptileFleetsMap.put(type, fleetsMap);
    }

    public Map<Class<? extends SpaceUnit>, UnitGroup> getFleetsByTypeAndLvl(BattleConfig battleConfig) {
        UnitDTO[] unitDTOs = reptileFleetsMap.get(battleConfig.getAttackType()).get(battleConfig.getComplexity().getComplexity()).getUnits();
        return createUnitGroup(unitDTOs, battleConfig);
    }

    public ResourceContainer getTradeFleetRewardByLvl(Integer lvl) {
        ReptileFleetDTO fleetDTO = reptileFleetsMap.get(AttackType.TRADE).get(lvl);
        return ResourceContainer.builder().metal(fleetDTO.getMetalReward().doubleValue())
                .crystal(fleetDTO.getCrystalReward().doubleValue())
                .build();
    }

    private Map<Class<? extends SpaceUnit>, UnitGroup> createUnitGroup(UnitDTO[] unitDTOS, BattleConfig battleConfig) {

        Map<Class<? extends SpaceUnit>, UnitGroup> unitGroups = new LinkedHashMap<>();
        for (UnitDTO unitDTO : unitDTOS) {
            Class<? extends SpaceUnit> unitType = NameToClassLinker.getInstance().getClassForName(unitDTO.getName());
            unitGroups.put(unitType, getUnitGroupByTypeAndAmount(unitType, unitDTO.getCount().getAmount(), battleConfig));
        }
        return unitGroups;
    }

    private UnitGroup getUnitGroupByTypeAndAmount(Class<? extends SpaceUnit> unitType, Long amount, BattleConfig battleConfig) {
        return UnitGroup.create(SpaceUnitCreationHelper.getInstance().getInstanceOf(unitType, battleConfig), amount);
    }
}
