package ua.lsi.consuls.core.ships;

import ua.lsi.consuls.core.enums.UnitType;

/**
 * @author ID69
 */
public class UnitGroup implements Cloneable {
    private SpaceUnit spaceUnit;
    private Long currentAttack;
    private Long currentHealth;
    private Long initialCount;
    private Long count;
    private Boolean isDead = false;
    private UnitType unitType;

    private UnitGroup(SpaceUnit spaceUnit, long count) {
        this.spaceUnit = spaceUnit;
        this.unitType = spaceUnit.getType();
        this.initialCount = count;
        this.count = count;
    }

    public static UnitGroup create(SpaceUnit spaceUnit, long count) {
        UnitGroup unitGroup = new UnitGroup(spaceUnit, count);
        unitGroup.calculateInitialAttackAndHealth();
        return unitGroup;
    }


    private void calculateInitialAttackAndHealth() {
        currentAttack = count * spaceUnit.getAttack();
        currentHealth = count * spaceUnit.getHealth();
    }

    public Long getCurrentAttack() {
        return currentAttack;
    }

    public void recalculateCurrentAttack() {
        currentAttack = count * spaceUnit.getAttack();
    }

    public Long getCurrentHealth() {
        return currentHealth;
    }

    public void setCurrentHealthAndRecalculateCount(Long health) {
        currentHealth = health;

        count = currentHealth / spaceUnit.getHealth();

        if (currentHealth % spaceUnit.getHealth() > 0) {
            count++;
        }

        if (count.equals(0L)) {
            isDead = true;
        }
    }

    public Class<? extends SpaceUnit> getUnitClass() {
        return spaceUnit.getClass();
    }

    public boolean isDead() {
        return isDead;
    }

    public Long dealDamageFromAttackerAndReturnDamageLeft(Float damageToDeal, UnitGroup attacker, int roundNumber) {
        int damageToDealWithModifier = Math.round(
                damageToDeal
                        * attacker.spaceUnit.getAttackModifierForEnemy(this.spaceUnit, roundNumber)
                        * this.spaceUnit.getDamageReceiveModifier(attacker.spaceUnit, roundNumber)
        );
        Long healthLeft = this.getCurrentHealth() - damageToDealWithModifier;
        logDamageInfo(attacker, roundNumber, damageToDealWithModifier, healthLeft);
        if (healthLeft < 0) {
            Integer damageToReturn =
                    Math.round(damageToDeal * (1 - (float) this.getCurrentHealth() / damageToDealWithModifier));
            this.setCurrentHealthAndRecalculateCount(0L);
            return Math.round(damageToReturn * spaceUnit.battleConfig.getTargetChangeModifierCoefficient());
        } else {
            this.setCurrentHealthAndRecalculateCount(healthLeft);
        }
        return 0L;
    }

    private void logDamageInfo(UnitGroup attacker, int roundNumber, int damageToDealWithModifier, Long healthLeft) {
        System.out.println("Round " + roundNumber + ": "
                + this.spaceUnit.getDisplayName() + " had " + this.getCurrentHealth() + " HP and received "
                + damageToDealWithModifier + " damage from " + attacker.spaceUnit.getDisplayName()
                + " so left with " + healthLeft + " HP");
    }

    public Long getCount() {
        return count;
    }

    public SpaceUnit getSpaceUnit() {
        return spaceUnit;
    }

    public UnitType getUnitType() {
        return unitType;
    }

    public Long getTotalMetalReward() {
        if (count == 0) {
            Long reward = 0L;
            reward += Math.round(initialCount * spaceUnit.getMetal());
            return reward;
        }
        return 0L;
    }

    public Long getTotalCrystalReward() {
        if (count == 0) {
            Long reward = 0L;
            reward += Math.round(initialCount * spaceUnit.getCrystal());
            return reward;
        }
        return 0L;
    }

    public Double getTotalHonorReward() {
        return getDeadCount() * spaceUnit.getHonorReward();
    }

    public Long getMetalLoses() {
        return Math.round(((getDeadCount()) * spaceUnit.getMetal()));
    }

    public Long getCrystalLoses() {
        return Math.round(((getDeadCount()) * spaceUnit.getCrystal()));
    }

    public Integer getTimeLoses() {
        return Math.round((getDeadCount()) * spaceUnit.getTime());
    }

    public Long getHumanLoses() {
        return Math.round((double) ((getDeadCount()) * spaceUnit.getHuman()));
    }

    public Long getMetalRepair() {
        return Math.round(((getDeadCount()) * spaceUnit.getRepairMetal()));
    }

    public Long getCrystalRepair() {
        return Math.round(((getDeadCount()) * spaceUnit.getRepairCrystal()));
    }

    public Long getHumanRepair() {
        return Math.round((double) ((getDeadCount()) * spaceUnit.getRepairHuman()));
    }

    private long getDeadCount() {
        return initialCount - count;
    }
    @Override
    public String toString() {
        return spaceUnit.getDisplayName();
    }

    @Override
    public UnitGroup clone() {
        UnitGroup clone;
        try {
            clone = (UnitGroup) super.clone();
        } catch (CloneNotSupportedException e) {
            System.out.println(e.getMessage());
            return null;
        }
        clone.spaceUnit = spaceUnit;
        clone.count = count;
        clone.currentAttack = currentAttack;
        clone.currentHealth = currentHealth;
        clone.initialCount = initialCount;
        clone.isDead = isDead;
        clone.unitType = unitType;
        return clone;
    }
}
