package ua.lsi.consuls.core.enums.flat;

/**
 * @author ID69
 */
public enum Avatar implements BonusItem {
    NO_BONUS("Без бонуса", false, 0),
    JOHN_SNOW("Аватар Джон Сноу", true, 2);

    private String rusName;
    private boolean isForHealth;
    private int percent;

    Avatar(String rusName, boolean isForHealth, int percent) {
        this.rusName = rusName;
        this.isForHealth = isForHealth;
        this.percent = percent;
    }

    public boolean isForHealth() {
        return isForHealth;
    }

    public int getPercent() {
        return percent;
    }

    @Override
    public String toString() {
        return rusName;
    }
}
