package ua.lsi.consuls.core.ships.reptile;

import ua.lsi.consuls.core.battle.BattleConfig;

/**
 * @author ID69
 */
public class Octopus extends ReptileShip {
    public static final String NAME = "Спрут";

    public Octopus(BattleConfig battleConfig) {
        super(battleConfig);
    }
}
