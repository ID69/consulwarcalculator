package ua.lsi.consuls.core.battle;

import lombok.Data;
import ua.lsi.consuls.core.enums.AttackType;
import ua.lsi.consuls.core.enums.BattleType;
import ua.lsi.consuls.core.model.ResourceContainer;
import ua.lsi.consuls.core.ships.SpaceUnit;
import ua.lsi.consuls.core.ships.UnitGroup;
import ua.lsi.consuls.core.util.ReptileFleetProvider;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Data
public class BattleCalculations {

    private Map<Class<? extends SpaceUnit>, UnitGroup> humanShips;
    private Map<Class<? extends SpaceUnit>, UnitGroup> reptileShips;
    private BattleConfig battleConfig;

    private String humanStatus;
    private String reptileStatus;
    private List<RoundResults> rounds;
    private ResourceContainer repair;
    private ResourceContainer reward;
    private ResourceContainer loses;
    private ResourceContainer profit;


    public BattleCalculations(Map<Class<? extends SpaceUnit>, UnitGroup> humanShips,
                              Map<Class<? extends SpaceUnit>, UnitGroup> reptileShips,
                              BattleConfig battleConfig) {
        this.humanShips = humanShips;
        this.reptileShips = reptileShips;
        this.battleConfig = battleConfig;
        rounds = new ArrayList<>();
    }

    public void makeBattle(BattleType battleType) {
        switch (battleType) {
            case EXTERMINATION:
                calculateBattle(this::makeExterminationBattle);
                break;
            case STANDART:
            default:
                calculateBattle(this::makeRegularBattle);
        }
        calculateResourceResults();
    }

    private void calculateBattle(Function<List<RoundResults>, List<RoundResults>> function) {
        formStatuses(humanShips, reptileShips);

        //Filling before battle stats
        RoundResults roundBefore = new RoundResults();
        roundBefore.fillRoundLabel(0, false);
        roundBefore.setHumanShips(recalculateAndReturnShips(humanShips));
        roundBefore.setReptileShips(recalculateAndReturnShips(reptileShips));

        rounds.add(roundBefore);

        rounds = function.apply(rounds);
    }

    private List<RoundResults> makeRegularBattle(List<RoundResults> rounds) {
        //actual rounds
        Integer roundNum = 1;
        boolean continueCalculation = true;
        while (continueCalculation) {
            RoundResults round = new RoundResults();

            makeMassAttackFromTo(humanShips, reptileShips, roundNum);
            makeMassAttackFromTo(reptileShips, humanShips, roundNum);

            round.setHumanShips(recalculateAndReturnShips(humanShips));
            round.setReptileShips(recalculateAndReturnShips(reptileShips));

            continueCalculation = !areAllShipGroupsDead(humanShips) && !areAllShipGroupsDead(reptileShips);

            round.fillRoundLabel(roundNum++, !continueCalculation);
            rounds.add(round);
        }
        return rounds;
    }

    private List<RoundResults> makeExterminationBattle(List<RoundResults> rounds) {
        RoundResults lastRound = new RoundResults();

        killAllGroups(humanShips);
        lastRound.setHumanShips(recalculateAndReturnShips(humanShips));
        killAllGroups(reptileShips);
        lastRound.setReptileShips(recalculateAndReturnShips(reptileShips));
        rounds.add(lastRound);
        return rounds;
    }

    private void formStatuses(Map<Class<? extends SpaceUnit>, UnitGroup> humanShips, Map<Class<? extends SpaceUnit>, UnitGroup> reptileShips) {
        humanStatus = createStatus(getSumHealth(humanShips), getSumDamage(humanShips));
        reptileStatus = createStatus(getSumHealth(reptileShips), getSumDamage(reptileShips));
    }

    private String createStatus(Long health, Long damage) {
        return "Сумма [ " + "ХП: " + health + ", Дамаг: " + damage + " ]";
    }

    private Long getSumHealth(Map<Class<? extends SpaceUnit>, UnitGroup> ships) {
        Long sum = 0L;
        for (UnitGroup unitGroup : ships.values()) {
            sum += unitGroup.getCurrentHealth();
        }
        return sum;
    }

    private Long getSumDamage(Map<Class<? extends SpaceUnit>, UnitGroup> ships) {
        Long sum = 0L;
        for (UnitGroup unitGroup : ships.values()) {
            unitGroup.recalculateCurrentAttack();
            sum += unitGroup.getCurrentAttack();
        }
        return sum;
    }

    private void killAllGroups(Map<Class<? extends SpaceUnit>, UnitGroup> shipGroupMap) {
        shipGroupMap.values().forEach(unitGroup -> unitGroup.setCurrentHealthAndRecalculateCount(0L));
    }

    private void makeMassAttackFromTo(Map<Class<? extends SpaceUnit>, UnitGroup> attacker,
                                      Map<Class<? extends SpaceUnit>, UnitGroup> targets,
                                      Integer roundNum) {
        for (UnitGroup ships : attacker.values()) {
            makeAttack(ships, targets, roundNum);
        }
    }

    private List<UnitGroup> recalculateAndReturnShips(Map<Class<? extends SpaceUnit>, UnitGroup> attacker) {
        List<UnitGroup> updatedShips = new ArrayList<>();
        for (UnitGroup ships : attacker.values()) {
            ships.recalculateCurrentAttack();
            updatedShips.add(ships.clone());
        }
        return updatedShips;
    }

    private void makeAttack(UnitGroup ships, Map<Class<? extends SpaceUnit>, UnitGroup> targets, Integer roundNum) {

        int damageLeftToDeal = 0;
        List<UnitGroup> aliveTargets = getAliveShipGroups(ships.getSpaceUnit().getAllPriorities(), targets);

        Map<Class<? extends SpaceUnit>, Float> targetToPercentMap = getTargetToPercentMap(ships, targets, aliveTargets, roundNum);

        for (UnitGroup target : aliveTargets) {
            Float percentOfDamage = targetToPercentMap.get(target.getUnitClass());
            damageLeftToDeal +=
                    target.dealDamageFromAttackerAndReturnDamageLeft(percentOfDamage * ships.getCurrentAttack(), ships, roundNum);
        }

        if (damageLeftToDeal > 0) {
            List<UnitGroup> shipsToAttackLeft = getAliveShipGroups(ships.getSpaceUnit().getAllPriorities(), targets);
            if (!shipsToAttackLeft.isEmpty()) {
                attackListedTargetsAndReturnRemainingDamage(damageLeftToDeal, ships, shipsToAttackLeft, roundNum);
            }
        }
    }

    private Map<Class<? extends SpaceUnit>, Float> getTargetToPercentMap(UnitGroup ships,
                                                                         Map<Class<? extends SpaceUnit>, UnitGroup> targets,
                                                                         List<UnitGroup> aliveTargets,
                                                                         Integer roundNum) {
        Map<Class<? extends SpaceUnit>, Float> targetToPercentMap = new LinkedHashMap<>();

        List<Class<? extends SpaceUnit>> mainPriorities = ships.getSpaceUnit().getMainPriorities();
        float initialDamagePercentLeftToSplit = 1f;
        for (UnitGroup target : aliveTargets) {
            Float targetTakesPercent = ships.getSpaceUnit().getPercentGivesFromAttack(target.getSpaceUnit(), roundNum);
            Float existingPercent = targetToPercentMap.getOrDefault(target.getUnitClass(), targetTakesPercent);
            initialDamagePercentLeftToSplit -= targetTakesPercent;
            targetToPercentMap.put(target.getUnitClass(), existingPercent);
        }
        float damagePercentLeftToSplit = initialDamagePercentLeftToSplit;
        if (damagePercentLeftToSplit < 0) {
            damagePercentLeftToSplit = 0;
            initialDamagePercentLeftToSplit = 0;
        }
        for (int i = 0; i < mainPriorities.size(); i++) {
            float percentOfDamage = (0.4f - 0.1f * i) * initialDamagePercentLeftToSplit;
            UnitGroup target = targets.get(mainPriorities.get(i));
            if (target == null || target.isDead()) {
                continue;
            }
            damagePercentLeftToSplit -= percentOfDamage;
            Float existingPercent = targetToPercentMap.getOrDefault(target.getUnitClass(), 0f);
            targetToPercentMap.put(target.getUnitClass(), existingPercent + percentOfDamage);
        }

        for (UnitGroup target : aliveTargets) {
            Float additionalPercents = damagePercentLeftToSplit / aliveTargets.size();
            Float existingPercent = targetToPercentMap.getOrDefault(target.getUnitClass(), 0f);
            targetToPercentMap.put(target.getUnitClass(), existingPercent + additionalPercents);
        }
        return targetToPercentMap;
    }

    private List<UnitGroup> getAliveShipGroups(List<Class<? extends SpaceUnit>> priorities, Map<Class<? extends SpaceUnit>, UnitGroup> targets) {
        List<UnitGroup> aliveSpaceUnitGroups = new ArrayList<>();
        priorities.stream()
                .filter(type -> targets.get(type) != null && !targets.get(type).isDead())
                .forEach(type -> aliveSpaceUnitGroups.add(targets.get(type)));
        return aliveSpaceUnitGroups;
    }

    private boolean areAllShipGroupsDead(Map<Class<? extends SpaceUnit>, UnitGroup> targets) {
        Long count = targets.values().stream()
                .filter(type -> type != null && !type.isDead())
                .count();
        return count.equals(0L);
    }


    private void attackListedTargetsAndReturnRemainingDamage(int damageToDeal, UnitGroup attacker, List<UnitGroup> targets, Integer roundNum) {
        int lastTargetCount = targets.size();
        for (UnitGroup target : targets) {
            Float damagePart = (float) damageToDeal / lastTargetCount;
            target.dealDamageFromAttackerAndReturnDamageLeft(damagePart, attacker, roundNum);
        }
    }

    private void calculateResourceResults() {
        Double metalReward = 0d;
        Double crystalReward = 0d;
        Double honorReward = getAllShips(reptileShips).stream().mapToDouble(UnitGroup::getTotalHonorReward).sum();

        if (areAllShipGroupsDead(reptileShips)) {
            for (UnitGroup ship : getAllShips(reptileShips)) {
                metalReward += ship.getTotalMetalReward();
                crystalReward += ship.getTotalCrystalReward();
            }
        }

        if (AttackType.TRADE.equals(battleConfig.getAttackType())) {
            ResourceContainer fleetReward = ReptileFleetProvider.getInstance().getTradeFleetRewardByLvl(battleConfig.getComplexity().getComplexity());
            metalReward = fleetReward.getMetal();
            crystalReward = fleetReward.getCrystal();
        }


        ResourceContainer reward = new ResourceContainer();
        reward.setMetal(metalReward);
        reward.setCrystal(crystalReward);
        reward.setHonor(honorReward * battleConfig.getAttackType().getHonorCoefficient());


        ResourceContainer loses = calculateLoses();
        ResourceContainer repair = calculateRepair();
        ResourceContainer profit = calculateProfit(reward, loses);

        this.reward = reward;
        this.loses = loses;
        this.repair = repair;
        this.profit = profit;
    }

    private ResourceContainer calculateProfit(ResourceContainer reward, ResourceContainer loses) {
        ResourceContainer profit = new ResourceContainer();
        profit.setMetal(reward.getMetal() - loses.getMetal());
        profit.setCrystal(reward.getCrystal() - loses.getCrystal());
        profit.setHonor(reward.getHonor() + (profit.getMetal() / 300f) + (profit.getCrystal() / 100f));
        return profit;
    }

    private ResourceContainer calculateLoses() {
        List<UnitGroup> allHumanShips = getAllShips(humanShips);
        ResourceContainer loses = new ResourceContainer();
        loses.setMetal(allHumanShips.stream().mapToDouble(UnitGroup::getMetalLoses).sum());
        loses.setCrystal(allHumanShips.stream().mapToDouble(UnitGroup::getCrystalLoses).sum());
        loses.setTime(allHumanShips.stream().mapToInt(UnitGroup::getTimeLoses).sum());
        loses.setHuman(allHumanShips.stream().mapToLong(UnitGroup::getHumanLoses).sum());
        return loses;
    }

    private ResourceContainer calculateRepair() {
        List<UnitGroup> allHumanShips = getAllShips(humanShips);
        ResourceContainer repair = new ResourceContainer();
        repair.setMetal(allHumanShips.stream().mapToDouble(UnitGroup::getMetalRepair).sum());
        repair.setCrystal(allHumanShips.stream().mapToDouble(UnitGroup::getCrystalRepair).sum());
        repair.setHuman(allHumanShips.stream().mapToLong(UnitGroup::getHumanRepair).sum());
        return repair;
    }

    private List<UnitGroup> getAllShips(Map<Class<? extends SpaceUnit>, UnitGroup> unitGroupMap) {
        return unitGroupMap.values().stream()
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }
}
