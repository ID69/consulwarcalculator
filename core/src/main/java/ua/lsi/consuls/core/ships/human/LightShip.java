package ua.lsi.consuls.core.ships.human;

import ua.lsi.consuls.core.battle.BattleConfig;

public abstract class LightShip extends HumanShip {

    protected LightShip(BattleConfig battleConfig) {
        super(battleConfig);
    }

    @Override
    protected Integer getTimeReduction0(Integer time){
        return (int) (time - time / (1 + (0.036 * battleConfig.getSpacePortLvl())));
    }
}
