package ua.lsi.consuls.core.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ID69
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UnitParameters {

    private String name;

    private String displayName;

    private Integer minAttack;

    private Integer maxAttack;

    private Integer health;

    private Integer signatureAttack;

    private Integer signatureHealth;

    private Integer technologyLvl;

    private Float power;

    private String[] priorities;

    private ResourceContainer price;
}
