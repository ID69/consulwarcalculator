package ua.lsi.consuls.core.util;

import ua.lsi.consuls.core.model.UnitParameters;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @author ID69
 */
public class UnitParamsProvider {

    private static class SingletonHolder {
        static final UnitParamsProvider INSTANCE = new UnitParamsProvider();
    }

    public static UnitParamsProvider getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private Map<String, UnitParameters> parametersMap = new HashMap<>();

    private UnitParamsProvider() {
        load("/units/Human.json", "/units/HumanDefence.json", "/units/Reptile.json");
    }

    public void load(String... files) {
        for (String fileName : files) {
            UnitParameters[] parameters = DataFileParser.getInstance().parse(fileName, UnitParameters[].class);
            Arrays.stream(parameters)
                    .forEach(unitParameters -> parametersMap.put(unitParameters.getName(), unitParameters));
        }
    }

    public UnitParameters getParametersByName(String name) {
        return parametersMap.get(name);
    }

    public String getDisplayNameByName(String name) {
        return parametersMap.get(name).getDisplayName();
    }
}
