package ua.lsi.consuls.core.ships.human;

import ua.lsi.consuls.core.battle.BattleConfig;

/**
 * @author ID69
 */
public class Frigate extends HeavyShip {
    public static final String NAME = "Фрегат";

    public Frigate(BattleConfig battleConfig) {
        super(battleConfig);
    }
}
