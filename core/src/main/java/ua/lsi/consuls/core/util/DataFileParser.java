package ua.lsi.consuls.core.util;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;

import java.io.IOException;

/**
 * @author ID69
 */
public class DataFileParser {

    private static class SingletonHolder {
        static final DataFileParser INSTANCE = new DataFileParser();
    }

    public static DataFileParser getInstance() {
        return SingletonHolder.INSTANCE;
    }

    private final ObjectMapper objectMapper;

    private DataFileParser() {
        objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
    }

    @SneakyThrows
    public <T> T parse(String fileName, Class<T> valueType){
        JsonFactory factory = new JsonFactory();
        JsonParser parser = factory.createParser(UnitParamsProvider.class.getResourceAsStream(fileName));
        return objectMapper.readValue(parser, valueType);
    }

}
