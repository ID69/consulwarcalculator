package ua.lsi.consuls.core.battle;

import lombok.Data;
import ua.lsi.consuls.core.ships.UnitGroup;

import java.util.List;

@Data
public class RoundResults {
    private String roundLabel;
    private List<UnitGroup> humanShips;
    private List<UnitGroup> reptileShips;

    public void fillRoundLabel(Integer number, boolean afterBattle) {
        if (!afterBattle) {
            roundLabel = "Раунд " + number;
        } else {
            roundLabel = "После боя";
        }
        if (number.equals(0)) {
            roundLabel = "До боя";
        }
    }
}
