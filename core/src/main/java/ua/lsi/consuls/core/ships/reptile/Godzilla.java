package ua.lsi.consuls.core.ships.reptile;

import ua.lsi.consuls.core.battle.BattleConfig;

/**
 * @author ID69
 */
public class Godzilla extends ReptileShip {
    public static final String NAME = "Годзила";

    public Godzilla(BattleConfig battleConfig) {
        super(battleConfig);
    }
}
