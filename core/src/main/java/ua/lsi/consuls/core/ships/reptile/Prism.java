package ua.lsi.consuls.core.ships.reptile;

import ua.lsi.consuls.core.battle.BattleConfig;

/**
 * @author ID69
 */
public class Prism extends ReptileShip {
    public static final String NAME = "Призма";

    public Prism(BattleConfig battleConfig) {
        super(battleConfig);
    }
}
