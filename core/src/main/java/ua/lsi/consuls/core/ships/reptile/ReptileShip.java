package ua.lsi.consuls.core.ships.reptile;

import ua.lsi.consuls.core.battle.BattleConfig;
import ua.lsi.consuls.core.enums.UnitType;
import ua.lsi.consuls.core.ships.SpaceUnit;
import ua.lsi.consuls.core.ships.human.*;
import ua.lsi.consuls.core.ships.human.defence.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ID69
 */
public abstract class ReptileShip extends SpaceUnit {
    private static final List<Class<? extends SpaceUnit>> allPriorities = new ArrayList<>(12);
    private static final int ALL_PRICE_MULTIPLIER = 200;

    static {
        allPriorities.add(Gammadrone.class);
        allPriorities.add(Wasp.class);
        allPriorities.add(Mirage.class);
        allPriorities.add(Frigate.class);
        allPriorities.add(Truckc.class);
        allPriorities.add(Cruiser.class);
        allPriorities.add(Battleship.class);
        allPriorities.add(Carrier.class);
        allPriorities.add(Dreadnought.class);
        allPriorities.add(Railgun.class);
        allPriorities.add(Reaper.class);
        allPriorities.add(Flagship.class);

        allPriorities.add(Bomb.class);
        allPriorities.add(IonBomb.class);
        allPriorities.add(Turret.class);
        allPriorities.add(LaserTurret.class);
        allPriorities.add(SniperGun.class);
        allPriorities.add(RailCannon.class);
        allPriorities.add(PlasmaKiller.class);
        allPriorities.add(Tyrant.class);
        allPriorities.add(CrystalGun.class);
        allPriorities.add(Trilinear.class);
        allPriorities.add(Deforbital.class);
//        allPriorities.add(DoomsDayGun.class);
    }

    public ReptileShip(BattleConfig battleConfig) {
        super(battleConfig);
    }

    @Override
    public List<Class<? extends SpaceUnit>> getAllPriorities() {
        return allPriorities;
    }

    @Override
    public UnitType getType() {
        return UnitType.REPTILE;
    }

    @Override
    public Double getMetal() {
        return parameters.getPrice().getMetal();
    }

    @Override
    public Double getCrystal() {
        return parameters.getPrice().getCrystal();
    }

    @Override
    public Double getHonorReward() {
        return parameters.getPrice().getMetal() * ALL_PRICE_MULTIPLIER / 300 + parameters.getPrice().getCrystal() * ALL_PRICE_MULTIPLIER / 100;
    }

    @Override
    public Integer getAttack() {
        return getMinAttack() + Double.valueOf((getMaxAttack() - getMinAttack()) * battleConfig.getReptileDamagePercent()).intValue();
    }
}
