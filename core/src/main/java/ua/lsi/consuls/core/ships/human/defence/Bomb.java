package ua.lsi.consuls.core.ships.human.defence;

import ua.lsi.consuls.core.battle.BattleConfig;

/**
 * @author ID69
 */
public class Bomb extends DefenceUnit {
    public static final String NAME = "Мины";

    public Bomb(BattleConfig battleConfig) {
        super(battleConfig);
    }
}