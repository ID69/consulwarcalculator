package ua.lsi.consuls.core.ships.human.defence;

import ua.lsi.consuls.core.battle.BattleConfig;

/**
 * @author ID69
 */
public class Turret extends DefenceUnit {
    public static final String NAME = "Турель";

    public Turret(BattleConfig battleConfig) {
        super(battleConfig);
    }
}