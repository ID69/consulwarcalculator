package ua.lsi.consuls.core.ships.human.defence;

import ua.lsi.consuls.core.battle.BattleConfig;

/**
 * @author ID69
 */
public class Tyrant extends DefenceUnit {
    public static final String NAME = "Тиран";

    public Tyrant(BattleConfig battleConfig) {
        super(battleConfig);
    }
}