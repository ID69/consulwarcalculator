package ua.lsi.consuls.core.battle;

import lombok.Data;
import ua.lsi.consuls.core.enums.AttackType;
import ua.lsi.consuls.core.enums.Complexity;
import ua.lsi.consuls.core.enums.flat.BonusItem;
import ua.lsi.consuls.core.ships.SpaceUnit;

import java.util.List;
import java.util.Map;

@Data
public class BattleConfig {
    private AttackType attackType;
    private Complexity complexity;
    private Map<Class<? extends SpaceUnit>, Integer> updatesMap;
    //donate
    private Boolean crazyDonateBonus;
    //achievements
    private Boolean satanMinister;
    private Boolean pirateRaid;
    private Boolean braveCaptain;
    private Boolean headlessAdmiral;
    private Boolean lepreconKiller;
    private Boolean coldbloodedCalmly;
    private Boolean lightBringer;
    private Boolean levTolstoy;
    //research
    private Integer energyLvl;
    private Integer alloyLvl;
    private Integer scienceLvl;
    private Integer ikeaLvl;
    private Integer defenseEngineeringLvl;
    private Integer hyperdriveLvl;
    private Integer nanotechnologyLvl;
    private Integer plasmoidConverterLvl;
    private Integer doomDayCalibrationLvl;
    //building residential
    private Integer spacePortLvl;
    private Integer entertainmentCenterLvl;
    private Integer blackMarketLvl;
    //building military
    private Integer barracksLvl;
    private Integer militaryFactoryLvl;
    private Integer airfieldLvl;
    private Integer shipyyardLvl;
    private Integer defenseComplexLvl;
    private Integer gatesLvl;
    private Integer engineeringComplexLvl;
    private Integer oscdFabricLvl;
    //room
    private List<BonusItem> bonusItems;
    //general damage
    private Double humanDamagePercent;
    private Double reptileDamagePercent;
    private Double targetChangeModifierCoefficient = 1d;
    //custom
    private Double attackUnitCustomPercent;
    private Double healthUnitCustomPercent;
    private Double attackDefenceCustomPercent;
    private Double healthDefenceCustomPercent;

    public Double getHumanAttackPercentBonus() {
        Double value = attackUnitCustomPercent;
        value += bonusItems.stream()
                .filter(bonusItem -> !bonusItem.isForHealth())
                .mapToInt(BonusItem::getPercent)
                .sum() / 100d;
        return value;
    }

    public Double getHumanHealthPercentBonus() {
        Double value = healthUnitCustomPercent;
        if (coldbloodedCalmly) {
            value += 0.05d;
        }
        value += bonusItems.stream()
                .filter(BonusItem::isForHealth)
                .mapToInt(BonusItem::getPercent)
                .sum() / 100d;

        return value;
    }


    public Double getDefenceAttackPercentBonus() {
        Double value = attackDefenceCustomPercent;
        value += defenseEngineeringLvl * 0.002d;
        return value;
    }

    public Double getDefenceHealthPercentBonus() {
        Double value = healthDefenceCustomPercent;
        value += oscdFabricLvl * 0.002d;
        return value;
    }

    public Double getFlagshipBonusDamage() {
        Double value = 0d;
        if (lightBringer) {
            value += 0.02d;
        }
        if (levTolstoy) {
            value += 0.10d;
        }
        return value;
    }

    public Integer getUpdateForShip(Class<? extends SpaceUnit> aClass) {
        return updatesMap.get(aClass);
    }
}
