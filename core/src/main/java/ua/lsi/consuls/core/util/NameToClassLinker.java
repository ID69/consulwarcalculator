package ua.lsi.consuls.core.util;

import ua.lsi.consuls.core.ships.SpaceUnit;
import ua.lsi.consuls.core.ships.human.*;
import ua.lsi.consuls.core.ships.human.defence.*;
import ua.lsi.consuls.core.ships.reptile.*;

import java.util.*;

/**
 * Created by ID69 on 02.07.2016.
 *
 * @author ID69
 */
public class NameToClassLinker {
    private static class Holder {
        static NameToClassLinker instance = new NameToClassLinker();
    }

    public static NameToClassLinker getInstance() {
        return Holder.instance;
    }

    private Map<String, Class<? extends SpaceUnit>> nameToClassMapShips = new LinkedHashMap<>();
    private Map<String, Class<? extends SpaceUnit>> nameToClassMapDefense = new LinkedHashMap<>();
    private Map<String, Class<? extends SpaceUnit>> nameToClassMapReptile = new LinkedHashMap<>();

    private NameToClassLinker() {
        nameToClassMapShips.put(Gammadrone.NAME, Gammadrone.class);
        nameToClassMapShips.put(Wasp.NAME, Wasp.class);
        nameToClassMapShips.put(Mirage.NAME, Mirage.class);
        nameToClassMapShips.put(Frigate.NAME, Frigate.class);
        nameToClassMapShips.put(Truckc.NAME, Truckc.class);
        nameToClassMapShips.put(Cruiser.NAME, Cruiser.class);
        nameToClassMapShips.put(Battleship.NAME, Battleship.class);
        nameToClassMapShips.put(Carrier.NAME, Carrier.class);
        nameToClassMapShips.put(Dreadnought.NAME, Dreadnought.class);
        nameToClassMapShips.put(Railgun.NAME, Railgun.class);
        nameToClassMapShips.put(Reaper.NAME, Reaper.class);
        nameToClassMapShips.put(Flagship.NAME, Flagship.class);

        nameToClassMapDefense.put(Bomb.NAME, Bomb.class);
        nameToClassMapDefense.put(IonBomb.NAME, IonBomb.class);
        nameToClassMapDefense.put(Turret.NAME, Turret.class);
        nameToClassMapDefense.put(LaserTurret.NAME, LaserTurret.class);
        nameToClassMapDefense.put(SniperGun.NAME, SniperGun.class);
        nameToClassMapDefense.put(RailCannon.NAME, RailCannon.class);
        nameToClassMapDefense.put(PlasmaKiller.NAME, PlasmaKiller.class);
        nameToClassMapDefense.put(Tyrant.NAME, Tyrant.class);
        nameToClassMapDefense.put(CrystalGun.NAME, CrystalGun.class);
        nameToClassMapDefense.put(Trilinear.NAME, Trilinear.class);
        nameToClassMapDefense.put(Deforbital.NAME, Deforbital.class);

        nameToClassMapReptile.put(Sphero.NAME, Sphero.class);
        nameToClassMapReptile.put(Blade.NAME, Blade.class);
        nameToClassMapReptile.put(Lacertian.NAME, Lacertian.class);
        nameToClassMapReptile.put(Wyvern.NAME, Wyvern.class);
        nameToClassMapReptile.put(Dragon.NAME, Dragon.class);
        nameToClassMapReptile.put(Hydra.NAME, Hydra.class);
        nameToClassMapReptile.put(Armadillo.NAME, Armadillo.class);
        nameToClassMapReptile.put(Prism.NAME, Prism.class);
        nameToClassMapReptile.put(Octopus.NAME, Octopus.class);
        nameToClassMapReptile.put(Godzilla.NAME, Godzilla.class);
        nameToClassMapReptile.put(Shadow.NAME, Shadow.class);
        nameToClassMapReptile.put(Trioniks.NAME, Trioniks.class);
    }

    public Collection<Class<? extends SpaceUnit>> getShipClasses() {
        return nameToClassMapShips.values();
    }

    public Collection<Class<? extends SpaceUnit>> getDefenseClasses() {
        return nameToClassMapDefense.values();
    }

    public Collection<Class<? extends SpaceUnit>> getReptileClasses() {
        return nameToClassMapReptile.values();
    }

    public Set<String> getNamesForShips() {
        return nameToClassMapShips.keySet();
    }

    public Set<String> getNamesForDefense() {
        return nameToClassMapDefense.keySet();
    }

    public String getNameForClass(Class<? extends SpaceUnit> aClass) {

        Optional<Map.Entry<String, Class<? extends SpaceUnit>>> name = findInMap(nameToClassMapShips, aClass);
        if (name.isPresent()) {
            return name.get().getKey();
        } else {
            name = findInMap(nameToClassMapDefense, aClass);
        }

        if (name.isPresent()) {
            return name.get().getKey();
        } else {
            name = findInMap(nameToClassMapReptile, aClass);
        }

        if (name.isPresent()) {
            return name.get().getKey();
        }

        throw new IllegalStateException();
    }

    private Optional<Map.Entry<String, Class<? extends SpaceUnit>>> findInMap(Map<String, Class<? extends SpaceUnit>> map, Class<? extends SpaceUnit> aClass) {
        return map.entrySet().stream().filter(stringClassEntry ->
                stringClassEntry.getValue().equals(aClass)
        ).findFirst();
    }

    public Class<? extends SpaceUnit> getClassForName(String name) {
        String displayName = UnitParamsProvider.getInstance().getDisplayNameByName(name);
        return getClassForDisplayName(displayName);
    }

    public Class<? extends SpaceUnit> getClassForDisplayName(String displayName) {
        Class<? extends SpaceUnit> aClass = nameToClassMapShips.get(displayName);
        if (aClass == null) {
            aClass = nameToClassMapDefense.get(displayName);
        }
        if (aClass == null) {
            aClass = nameToClassMapReptile.get(displayName);
        }
        return aClass;
    }
}
