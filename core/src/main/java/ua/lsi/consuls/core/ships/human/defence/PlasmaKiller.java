package ua.lsi.consuls.core.ships.human.defence;

import ua.lsi.consuls.core.battle.BattleConfig;

/**
 * @author ID69
 */
public class PlasmaKiller extends DefenceUnit {
    public static final String NAME = "Убийца";

    public PlasmaKiller(BattleConfig battleConfig) {
        super(battleConfig);
    }
}