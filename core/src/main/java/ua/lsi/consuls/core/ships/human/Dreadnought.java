package ua.lsi.consuls.core.ships.human;

import ua.lsi.consuls.core.battle.BattleConfig;
import ua.lsi.consuls.core.util.Formulas;

/**
 * @author ID69
 */
public class Dreadnought extends HeavyShip {
    public static final String NAME = "Дредноут";

    public Dreadnought(BattleConfig battleConfig) {
        super(battleConfig);
    }

    @Override
    protected Integer getTimeReduction1(Integer time) {
        return (int) (time - time / (1 + (Formulas.tier2(battleConfig.getMilitaryFactoryLvl()))));
    }

    @Override
    protected Integer getTimeReduction2(Integer time) {
        return (int) (time - time / (1 + (0.01 * battleConfig.getDefenseEngineeringLvl())));
    }
}
