package ua.lsi.consuls.core.ships.human;

import ua.lsi.consuls.core.battle.BattleConfig;

/**
 * @author ID69
 */
public class Flagship extends HumanShip {
    public static final String NAME = "Флагман";

    public Flagship(BattleConfig battleConfig) {
        super(battleConfig);
    }

    @Override
    protected Integer getZeroLvlAttackBonus(Integer attack) {
        return super.getZeroLvlAttackBonus(attack) + getAttackBonus();
    }

    @Override
    protected Integer getFirstLvlAttackBonus(Integer attack) {
        return super.getFirstLvlAttackBonus(attack) + (attack / 100 * battleConfig.getDoomDayCalibrationLvl()) + ((int) (attack * battleConfig.getFlagshipBonusDamage()));
    }

    private Integer getAttackBonus() {
        Integer bonus = 0;
        if (battleConfig.getPirateRaid()) {
            bonus += 20000;
        }
        if (battleConfig.getBraveCaptain()) {
            bonus += 30000;
        }
        if (battleConfig.getHeadlessAdmiral()) {
            bonus += 50000;
        }
        return bonus;
    }

    @Override
    protected Integer getBaseHealth() {
        return super.getBaseHealth() + getHealthBonus();
    }

    @Override
    public Integer getTime() {
        Integer lvl0 = price.getTime();
        Integer lvl1 = lvl0 - getTimeReduction0(lvl0);
        Integer lvl2 = lvl1 - getTimeReduction1(lvl1);
        Integer lvl3 = lvl2 - getTimeReduction2(lvl2);
        Integer lvl4 = lvl3 - getTimeReduction3(lvl3);
        return lvl4;
    }

    private Integer getHealthBonus() {
        Integer bonus = 0;
        if (battleConfig.getLepreconKiller()) {
            bonus += 50000;
        }
        return bonus;
    }
}
