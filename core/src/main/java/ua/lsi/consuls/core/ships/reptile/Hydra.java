package ua.lsi.consuls.core.ships.reptile;

import ua.lsi.consuls.core.battle.BattleConfig;

/**
 * @author ID69
 */
public class Hydra extends ReptileShip {
    public static final String NAME = "Гидра";

    public Hydra(BattleConfig battleConfig) {
        super(battleConfig);
    }
}
