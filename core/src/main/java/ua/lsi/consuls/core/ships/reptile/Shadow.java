package ua.lsi.consuls.core.ships.reptile;

import ua.lsi.consuls.core.battle.BattleConfig;

/**
 * @author ID69
 */
public class Shadow extends ReptileShip {
    public static final String NAME = "Тень";

    public Shadow(BattleConfig battleConfig) {
        super(battleConfig);
    }
}
