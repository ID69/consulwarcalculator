package ua.lsi.consuls.core.ships.human.defence;

import ua.lsi.consuls.core.battle.BattleConfig;

/**
 * @author ID69
 */
public class IonBomb extends DefenceUnit {
    public static final String NAME = "Ионные Мины";

    public IonBomb(BattleConfig battleConfig) {
        super(battleConfig);
    }
}