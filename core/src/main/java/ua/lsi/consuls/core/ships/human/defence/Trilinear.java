package ua.lsi.consuls.core.ships.human.defence;

import ua.lsi.consuls.core.battle.BattleConfig;

/**
 * @author ID69
 */
public class Trilinear extends DefenceUnit {
    public static final String NAME = "ТКГ";

    public Trilinear(BattleConfig battleConfig) {
        super(battleConfig);
    }
}