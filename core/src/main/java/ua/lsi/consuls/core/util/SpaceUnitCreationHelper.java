package ua.lsi.consuls.core.util;

import ua.lsi.consuls.core.battle.BattleConfig;
import ua.lsi.consuls.core.ships.SpaceUnit;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

/**
 * @author ID69
 */
public class SpaceUnitCreationHelper {
    private SpaceUnitCreationHelper() {
    }

    private static class Holder {
        static SpaceUnitCreationHelper instance = new SpaceUnitCreationHelper();
    }

    public static SpaceUnitCreationHelper getInstance() {
        return Holder.instance;
    }


    public SpaceUnit getInstanceOf(Class<? extends SpaceUnit> aClass, BattleConfig battleConfig) {
        SpaceUnit spaceUnit = null;
        try {
            Constructor constructor = aClass.getConstructor(BattleConfig.class);
            spaceUnit = (SpaceUnit) constructor.newInstance(battleConfig);
        } catch (NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return spaceUnit;
    }
}
