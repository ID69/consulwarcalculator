package ua.lsi.consuls.core.enums;

/**
 * @author ID69
 */
public enum AttackType {
    PATROL("Патруль", 0.3f),
    DEFENCE("Ряды Обороны", 0.3f),
    BATTLE("Боевой Флот", 0.3f),
    TRADE("Караван", 0.1f),
    IAMREADY("/яготов", 0.1f),
    PRISON("Корабль-тюрьма", 0.1f);

    private String name;
    private float honorCoefficient;

    AttackType(String name, float honorCoefficient) {
        this.name = name;
        this.honorCoefficient = honorCoefficient;

    }

    public float getHonorCoefficient() {
        return honorCoefficient;
    }

    @Override
    public String toString() {
        return name;
    }
}
