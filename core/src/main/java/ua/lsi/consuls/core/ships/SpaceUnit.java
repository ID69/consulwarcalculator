package ua.lsi.consuls.core.ships;

import ua.lsi.consuls.core.battle.BattleConfig;
import ua.lsi.consuls.core.enums.UnitType;
import ua.lsi.consuls.core.model.UnitParameters;
import ua.lsi.consuls.core.util.NameToClassLinker;
import ua.lsi.consuls.core.util.UnitParamsProvider;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ID69
 */
public abstract class SpaceUnit {

    protected UnitParameters parameters;
    protected BattleConfig battleConfig;

    protected SpaceUnit(BattleConfig battleConfig) {
        this.battleConfig = battleConfig;
        parameters = UnitParamsProvider.getInstance().getParametersByName(this.getClass().getSimpleName());
    }

    public Integer getAttack() {
        return parameters.getMinAttack();
    }

    public Integer getHealth() {
        return parameters.getHealth();
    }

    protected Integer getMinAttack() {
        return parameters.getMinAttack();
    }

    protected Integer getMaxAttack() {
        return parameters.getMaxAttack();
    }

    protected Integer getZeroLvlAttackBonus(Integer attack) {
        return attack;
    }

    protected Integer getFirstLvlAttackBonus(Integer attack) {
        return attack;
    }

    protected Integer getSecondLvlAttackBonus(Integer attack) {
        return attack;
    }

    protected Integer getBaseHealth() {
        return parameters.getHealth();
    }

    private Integer getSignatureHealth() {
        return parameters.getSignatureHealth();
    }

    private Integer getSignatureAttack() {
        return parameters.getSignatureAttack();
    }

    public List<Class<? extends SpaceUnit>> getMainPriorities() {
        List<Class<? extends SpaceUnit>> priorities = new ArrayList<>();
        for (String shipName : parameters.getPriorities()) {
            priorities.add(NameToClassLinker.getInstance().getClassForName(shipName));
        }
        return priorities;
    }

    public abstract List<Class<? extends SpaceUnit>> getAllPriorities();

    public Float getAttackModifierForEnemy(SpaceUnit target, int roundNumber) {
        if (target.getSignatureHealth() > this.getSignatureAttack()) {
            return 1.0f;
        }
        return target.getSignatureHealth().floatValue() / this.getSignatureAttack();
    }

    public Float getDamageReceiveModifier(SpaceUnit attacker, int roundNumber) {
        return 1.0f;
    }

    public Float getPercentTakesFromAttack(SpaceUnit attacker, int roundNumber) {
        return 0f;
    }

    public Float getPercentGivesFromAttack(SpaceUnit target, int roundNumber) {
        return target.getPercentTakesFromAttack(this, roundNumber);
    }

    public Double getMetal() {
        return 0d;
    }

    public Double getCrystal() {
        return 0d;
    }

    public Integer getTime() {
        return 0;
    }

    public Long getHuman() {
        return 0L;
    }


    public Double getRepairMetal() {
        return 0d;
    }

    public Double getRepairCrystal() {
        return 0d;
    }

    public Long getRepairHuman() {
        return 0L;
    }


    public Double getHonorReward() {
        return 0d;
    }

    public String getName() {
        return parameters.getName();
    }

    public String getDisplayName() {
        return parameters.getDisplayName();
    }

    public abstract UnitType getType();
}
