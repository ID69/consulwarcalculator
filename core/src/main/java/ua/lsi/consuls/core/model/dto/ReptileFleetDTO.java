package ua.lsi.consuls.core.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ID69
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ReptileFleetDTO {

    private Integer lvl;

    private UnitDTO[] units;

    private Long metalReward;

    private Long crystalReward;

    private Long humanReward;

}
