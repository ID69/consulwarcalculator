package ua.lsi.consuls.core.ships.human;

import ua.lsi.consuls.core.battle.BattleConfig;
import ua.lsi.consuls.core.enums.UnitType;
import ua.lsi.consuls.core.model.ResourceContainer;
import ua.lsi.consuls.core.ships.SpaceUnit;
import ua.lsi.consuls.core.ships.reptile.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ID69
 */
public abstract class HumanShip extends SpaceUnit {
    static final int UPDATE_STEP = 50;
    private static final List<Class<? extends SpaceUnit>> allPriorities = new ArrayList<>(12);
    private static final Float[] shipyyardBonuses = {0f, 0.05f, 0.1f, 0.2f, 0.3f, 0.4f};
    private Integer updateLvl;
    protected ResourceContainer price;

    static {
        allPriorities.add(Sphero.class);
        allPriorities.add(Blade.class);
        allPriorities.add(Lacertian.class);
        allPriorities.add(Wyvern.class);
        allPriorities.add(Trioniks.class);
        allPriorities.add(Dragon.class);
        allPriorities.add(Hydra.class);
        allPriorities.add(Armadillo.class);
        allPriorities.add(Prism.class);
        allPriorities.add(Octopus.class);
        allPriorities.add(Godzilla.class);
        allPriorities.add(Shadow.class);
    }

    protected HumanShip(BattleConfig battleConfig) {
        super(battleConfig);
        updateLvl = battleConfig.getUpdateForShip(this.getClass());
        this.price = parameters.getPrice();
    }

    @Override
    public Integer getAttack() {
        int attack = getMinAttack() + Double.valueOf((getMaxAttack() - getMinAttack()) * battleConfig.getHumanDamagePercent()).intValue();

        int attackZeroLvlBonuses = getZeroLvlAttackBonus(attack);
        int attackFirstLvlBonuses = getFirstLvlAttackBonus(attackZeroLvlBonuses);
        int attackSecondLvlBonuses = getSecondLvlAttackBonus(attackFirstLvlBonuses);
        int attackThirdLvlBonuses = attackSecondLvlBonuses + (int) (attackSecondLvlBonuses * battleConfig.getHumanAttackPercentBonus());

        return attackThirdLvlBonuses;
    }

    @Override
    protected Integer getFirstLvlAttackBonus(Integer attack) {
        float bonusHonor = getHonorAttack();

        int bonusSatan = 0;
        if (battleConfig.getSatanMinister()) {
            bonusSatan = attack / 100;
        }

        return attack + Float.valueOf(attack * bonusHonor).intValue() + bonusSatan;
    }

    @Override
    protected Integer getSecondLvlAttackBonus(Integer attack) {
        int bonusDonateAttack = 0;
        if (battleConfig.getCrazyDonateBonus()) {
            bonusDonateAttack = (int) (attack * 0.15f);
        }
        return attack + bonusDonateAttack;
    }

    @Override
    public Integer getHealth() {
        int health = getBaseHealth();
        int bonusDonateHealth = 0;
        float bonusHonor = getHonorHealth();
        int percentBonus;

        int healthZeroLvlBonuses = health + Float.valueOf(health * bonusHonor).intValue();
        int healthFirstLvlBonuses;
        int healthSecondLvlBonuses;

        percentBonus = (int) (health * battleConfig.getHumanHealthPercentBonus());

        healthFirstLvlBonuses = healthZeroLvlBonuses + percentBonus;

        if (battleConfig.getCrazyDonateBonus()) {
            bonusDonateHealth = healthFirstLvlBonuses / 5;
        }

        healthSecondLvlBonuses = healthFirstLvlBonuses + bonusDonateHealth;
        return healthSecondLvlBonuses;
    }

    @Override
    public List<Class<? extends SpaceUnit>> getAllPriorities() {
        return allPriorities;
    }

    private Float getHonorAttack() {
        return updateLvl * 0.004f;
    }

    private Float getHonorHealth() {
        return updateLvl * 0.004f;
    }

    @Override
    public UnitType getType() {
        return UnitType.HUMAN;
    }

    @Override
    public Double getMetal() {
        return price.getMetal()
                - (price.getMetal() * 0.001 * battleConfig.getBlackMarketLvl())
                - (price.getMetal() * 0.002 * battleConfig.getPlasmoidConverterLvl());
    }

    @Override
    public Double getCrystal() {
        return price.getCrystal()
                - (price.getCrystal() * 0.001 * battleConfig.getBlackMarketLvl())
                - (price.getCrystal() * 0.002 * battleConfig.getPlasmoidConverterLvl());
    }

    @Override
    public Integer getTime() {
        Integer lvl0 = price.getHumanTime();
        Integer lvl1 = lvl0 - getTimeReduction0(lvl0);
        Integer lvl2 = lvl1 - getTimeReduction1(lvl1);
        Integer lvl3 = lvl2 - getTimeReduction2(lvl2);
        Integer lvl4 = lvl3 - getTimeReduction3(lvl3);
        return lvl4;
    }

    protected Integer getTimeReduction0(Integer time) {
        return 0;
    }

    protected Integer getTimeReduction1(Integer time) {
        return 0;
    }

    protected Integer getTimeReduction2(Integer time) {
        return 0;
    }

    protected Integer getTimeReduction3(Integer time) {
        if (battleConfig.getCrazyDonateBonus()) {
            return (time - time / 2);
        }
        return 0;
    }

    @Override
    public Long getHuman() {
        return price.getHuman() - (int) (price.getHuman() * 0.002 * battleConfig.getEntertainmentCenterLvl());
    }

    @Override
    public Double getRepairMetal() {
        return price.getMetal() * 2 - (price.getMetal() * 2 * 0.003 * battleConfig.getEngineeringComplexLvl());
    }

    @Override
    public Double getRepairCrystal() {
        return price.getCrystal() * 2 - (price.getCrystal() * 2 * 0.003 * battleConfig.getEngineeringComplexLvl());
    }

    @Override
    public Long getRepairHuman() {
        return price.getHuman() * 2 - (int) (price.getHuman() * 2 * 0.003 * battleConfig.getEngineeringComplexLvl());
    }

    @Override
    public Float getAttackModifierForEnemy(SpaceUnit target, int roundNumber) {
        return super.getAttackModifierForEnemy(target, roundNumber);
    }

    Integer getUpdateLvl() {
        return updateLvl;
    }
}
