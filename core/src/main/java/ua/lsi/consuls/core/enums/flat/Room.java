package ua.lsi.consuls.core.enums.flat;

/**
 * @author ID69
 */
public enum Room implements BonusItem {
    NO_BONUS("Без бонуса", false, 0),
    GAME_OF_THRONES("Тронный Зал Игра Престолов", true, 3),
    CRUEL("Тронный Зал Жестокости", false, 2);

    private String rusName;
    private boolean isForHealth;
    private int percent;

    Room(String rusName, boolean isForHealth, int percent) {
        this.rusName = rusName;
        this.isForHealth = isForHealth;
        this.percent = percent;
    }

    public boolean isForHealth() {
        return isForHealth;
    }

    public int getPercent() {
        return percent;
    }

    @Override
    public String toString() {
        return rusName;
    }
}
