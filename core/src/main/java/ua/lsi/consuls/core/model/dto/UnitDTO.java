package ua.lsi.consuls.core.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ua.lsi.consuls.core.enums.UnitCount;

/**
 * @author ID69
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UnitDTO {

    private String name;

    private UnitCount count;
}
