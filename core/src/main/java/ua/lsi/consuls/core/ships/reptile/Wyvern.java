package ua.lsi.consuls.core.ships.reptile;

import ua.lsi.consuls.core.battle.BattleConfig;

/**
 * @author ID69
 */
public class Wyvern extends ReptileShip {
    public static final String NAME = "Виверна";

    public Wyvern(BattleConfig battleConfig) {
        super(battleConfig);
    }
}
