package ua.lsi.consuls.core.ships.reptile;

import ua.lsi.consuls.core.battle.BattleConfig;

/**
 * @author ID69
 */
public class Blade extends ReptileShip {
    public static final String NAME = "Клинок";

    public Blade(BattleConfig battleConfig) {
        super(battleConfig);
    }
}
