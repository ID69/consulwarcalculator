package ua.lsi.consuls.core.enums.flat;

/**
 * @author ID69
 */
public enum Tron implements BonusItem {
    NO_BONUS("Без бонуса", false, 0),
    GAME_OF_THRONES("Трон Игра Престолов", true, 2);

    private String rusName;
    private boolean isForHealth;
    private int percent;

    Tron(String rusName, boolean isForHealth, int percent) {
        this.rusName = rusName;
        this.isForHealth = isForHealth;
        this.percent = percent;
    }

    public boolean isForHealth() {
        return isForHealth;
    }

    public int getPercent() {
        return percent;
    }

    @Override
    public String toString() {
        return rusName;
    }
}
