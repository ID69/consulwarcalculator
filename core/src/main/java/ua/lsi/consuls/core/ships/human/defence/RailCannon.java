package ua.lsi.consuls.core.ships.human.defence;

import ua.lsi.consuls.core.battle.BattleConfig;

/**
 * @author ID69
 */
public class RailCannon extends DefenceUnit {
    public static final String NAME = "Рельсовая Пушка";

    public RailCannon(BattleConfig battleConfig) {
        super(battleConfig);
    }
}