package ua.lsi.consuls.core.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

/**
 * @author ID69
 */
@Data
@Builder
@AllArgsConstructor
public class ResourceContainer {
    private Long human;
    private Double crystal;
    private Double metal;
    private Double honor;
    private Integer time;

    public ResourceContainer() {
        this(0d, 0d, 0d);
    }

    public ResourceContainer(Double crystal, Double metal, Double honor) {
        this(0L, crystal, metal, honor, 0);
    }

    public Integer getHumanTime() {
        return (int) ((human * 4 + metal + crystal * 3) / 0.24);
    }
}
