package ua.lsi.consuls.core.enums;

import lombok.Getter;

/**
 * @author ID69
 */
public enum UnitCount {

    ONE(1L),          //Один
    FEW(4L),          //Несколько
    SEVERAL(9L),      //Немного
    PACK(19L),        //Отряд
    LOTS(49L),        //Толпа
    HORDE(99L),       //Орда
    THRONG(249L),     //Множество
    SWARM(499L),      //Туча
    ZOUNDS(999L),     //Полчище
    LEGION(4_999L),   //Легион
    DIVISION(9_999L), //Дивизия
    CORPS(19_999L),   //Корпус
    ARMY(44_999L),    //Армия
    GROUP(99_999L),   //Группа Армий
    FRONT(249_999L),   //Фронт
    AMOUNT_2(2L),
    AMOUNT_8(8L),
    AMOUNT_16(16L),
    AMOUNT_32(32L),
    AMOUNT_64(64L);

    @Getter
    private Long amount;

    UnitCount(Long amount) {
        this.amount = amount;
    }

}
