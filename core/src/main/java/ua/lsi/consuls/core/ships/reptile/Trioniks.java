package ua.lsi.consuls.core.ships.reptile;

import ua.lsi.consuls.core.battle.BattleConfig;

/**
 * @author ID69
 */
public class Trioniks extends ReptileShip {
    public static final String NAME = "Трионикс";

    public Trioniks(BattleConfig battleConfig) {
        super(battleConfig);
    }
}
