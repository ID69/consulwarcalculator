package ua.lsi.consuls.core.ships.reptile;

import ua.lsi.consuls.core.battle.BattleConfig;

/**
 * @author ID69
 */
public class Dragon extends ReptileShip {
    public static final String NAME = "Дракон";

    public Dragon(BattleConfig battleConfig) {
        super(battleConfig);
    }
}
