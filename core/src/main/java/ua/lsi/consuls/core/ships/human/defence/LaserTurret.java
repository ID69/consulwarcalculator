package ua.lsi.consuls.core.ships.human.defence;

import ua.lsi.consuls.core.battle.BattleConfig;

/**
 * @author ID69
 */
public class LaserTurret extends DefenceUnit {
    public static final String NAME = "Лазерная Турель";

    public LaserTurret(BattleConfig battleConfig) {
        super(battleConfig);
    }
}