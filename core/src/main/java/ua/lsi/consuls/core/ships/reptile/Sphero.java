package ua.lsi.consuls.core.ships.reptile;

import ua.lsi.consuls.core.battle.BattleConfig;

/**
 * @author ID69
 */
public class Sphero extends ReptileShip {
    public static final String NAME = "Сферо";

    public Sphero(BattleConfig battleConfig) {
        super(battleConfig);
    }
}
