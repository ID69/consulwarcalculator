package ua.lsi.consuls.core.enums;

/**
 * @author ID69
 */
public enum UnitType {
    REPTILE,
    HUMAN,
    HUMAN_DEFENCE
}
