package ua.lsi.consuls.core.util;

public class Formulas {

    public static Double tier2(int e) {
        return (e + (e >= 20 ? 100 * Math.ceil(Math.pow(1.31607, Math.floor(e / 20) - 1)) : 0)) / 2.5 / 100;
    }
}
