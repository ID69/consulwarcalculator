package ua.lsi.consuls.core.enums.flat;

/**
 * @author ID69
 */
public interface BonusItem {

    boolean isForHealth();

    int getPercent();
}
