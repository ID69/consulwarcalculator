package ua.lsi.consuls.standalone.controller;

import javafx.scene.Node;
import javafx.stage.Stage;

/**
 * @author ID69
 */
public abstract class AbstractController implements Controller {
    private Node view;
    private Stage primaryStage;

    public Node getView() {
        return view;
    }

    public void setView(Node view) {
        this.view = view;
    }

    @Override
    public void setStage(Stage primaryStage) {
        this.primaryStage = primaryStage;
    }

    @Override
    public Stage getStage() {
        return primaryStage;
    }

    @Override
    public void postInitialization() {

    }
}
