package ua.lsi.consuls.standalone.controller;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import ua.lsi.consuls.core.ships.UnitGroup;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * @author ID69
 */
public class ShipGroupPaneController extends AbstractController implements Initializable {
    @FXML
    Label nameLabel;
    @FXML
    Label countLabel;
    @FXML
    Label damageLabel;
    @FXML
    Label healthLabel;
    @FXML
    Label additionalInformationLabel;

    public void fillWithShips(UnitGroup ships) {
        nameLabel.setText(ships.getSpaceUnit().getDisplayName());
        countLabel.setText(ships.getCount().toString());
        healthLabel.setText(ships.getCurrentHealth().toString());
        damageLabel.setText(ships.getCurrentAttack().toString());
//        String additionalInformation = ships.getSpaceUnit().getAdditionalInfo();
//        if (!additionalInformation.isEmpty()) {
//            additionalInformationLabel.setText("?");
//            additionalInformationLabel.setTooltip(new Tooltip(additionalInformation));
//        }
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
//        getView().getStyleClass().add("-fx-border-color:black");
    }
}
