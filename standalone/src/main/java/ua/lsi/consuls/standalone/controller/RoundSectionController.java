package ua.lsi.consuls.standalone.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import ua.lsi.consuls.core.battle.RoundResults;
import ua.lsi.consuls.core.enums.UnitType;
import ua.lsi.consuls.core.ships.UnitGroup;

import java.io.IOException;
import java.io.InputStream;

/**
 * @author ID69
 */
public class RoundSectionController extends AbstractController {

    @FXML
    Label roundLabel;
    @FXML
    GridPane humanSide;
    @FXML
    GridPane reptileSide;
    @FXML
    HBox hboxContainer;
    @FXML
    BorderPane mainBorderContainer;

    public void createColumns() {
        int column = reptileSide.getChildren().size();
        reptileSide.addColumn(column, createShipGroupPaneController().getView());
        int column2 = humanSide.getChildren().size();
        humanSide.addColumn(column2, createShipGroupPaneController().getView());
    }

    public void fillRound(RoundResults roundResults){
        roundLabel.setText(roundResults.getRoundLabel());
        roundResults.getHumanShips().forEach(this::addShips);
        roundResults.getReptileShips().forEach(this::addShips);
    }

    public void addShips(UnitGroup ships) {
        if (ships.getUnitType().equals(UnitType.REPTILE)) {
            addNext(reptileSide, ships);
        } else {
            addNext(humanSide, ships);
        }
    }

    private void addNext(GridPane pane, UnitGroup ships) {
        int column = pane.getChildren().size();
        VBox node = createAndFillShipGroupPane(ships);
        Label nameLabel = (Label) node.lookup("#nameLabel");
        node.prefWidthProperty().bind(nameLabel.prefWidthProperty());
        pane.addColumn(column, node);
    }

    private VBox createAndFillShipGroupPane(UnitGroup ships) {
        ShipGroupPaneController controller = createShipGroupPaneController();
        controller.fillWithShips(ships);
        return (VBox) controller.getView();
    }

    private ShipGroupPaneController createShipGroupPaneController() {
        ShipGroupPaneController controller;
        try (InputStream fxmlStream = MainFormController.class.getResourceAsStream("/view/ShipGroupPane.fxml")) {
            FXMLLoader loader = new FXMLLoader();
            Parent view = loader.load(fxmlStream);

            controller = loader.getController();
            controller.setView(view);
            controller.postInitialization();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return controller;
    }
}
