package ua.lsi.consuls.standalone;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import ua.lsi.consuls.standalone.controller.Controller;
import ua.lsi.consuls.core.enums.AttackType;
import ua.lsi.consuls.core.util.ReptileFleetProvider;
import ua.lsi.consuls.core.util.UnitParamsProvider;

import javax.swing.*;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author ID69
 */
public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    private JDialog dialog;

    @Override
    public void start(Stage primaryStage) {

        try (InputStream fxmlStream = Main.class.getResourceAsStream("/view/MainForm.fxml")) {
            FXMLLoader loader = new FXMLLoader();

            Parent view = loader.load(fxmlStream);
            Controller controller = loader.getController();
            controller.setView(view);
            controller.setStage(primaryStage);
            Scene scene = new Scene(view);
            primaryStage.setTitle("Consulwar Space Battle Calculator");
            primaryStage.setScene(scene);
            primaryStage.getIcons().add(new Image(Main.class.getResourceAsStream("/icon16x16.png")));
            primaryStage.show();

            controller.postInitialization();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
