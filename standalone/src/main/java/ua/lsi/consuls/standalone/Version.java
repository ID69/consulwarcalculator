package ua.lsi.consuls.standalone;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * @author ID69
 */
public class Version {
    private static String version;

    public static String getVersion() {
        if (version == null) {
            version = loadVersion();
        }
        return version;
    }

    private static String loadVersion() {
        Properties properties = new Properties();
        try (InputStream inputStream = Version.class.getClassLoader().getResourceAsStream("version.properties")) {
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties.getProperty("version");
    }
}
