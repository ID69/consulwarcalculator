package ua.lsi.consuls.standalone.controller;

import javafx.application.Platform;
import javafx.beans.binding.DoubleBinding;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import ua.lsi.consuls.standalone.Version;
import ua.lsi.consuls.core.battle.BattleCalculations;
import ua.lsi.consuls.core.battle.BattleConfig;
import ua.lsi.consuls.core.enums.AttackType;
import ua.lsi.consuls.core.enums.BattleType;
import ua.lsi.consuls.core.enums.Complexity;
import ua.lsi.consuls.core.enums.flat.Avatar;
import ua.lsi.consuls.core.enums.flat.Room;
import ua.lsi.consuls.core.enums.flat.Tron;
import ua.lsi.consuls.core.ships.SpaceUnit;
import ua.lsi.consuls.core.ships.UnitGroup;
import ua.lsi.consuls.core.util.NameToClassLinker;
import ua.lsi.consuls.core.util.ReptileFleetProvider;
import ua.lsi.consuls.standalone.util.SettingsProvider;
import ua.lsi.consuls.core.util.SpaceUnitCreationHelper;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.*;

/**
 * @author ID69
 */
public class MainFormController extends AbstractController implements Initializable {
    public static Boolean windowAlwaysOnTop = false;

    @FXML
    CheckBox alwaysOnTop;

    @FXML
    GridPane humanShipsInputPane;

    @FXML
    GridPane humanShipsUpdatesPane;

    @FXML
    ComboBox<AttackType> attackComboBox;

    @FXML
    ComboBox<Complexity> complexityComboBox;

    //buildings
    //residential
    @FXML
    Label spacePortLabel;
    @FXML
    Spinner<Integer> spacePortSpinner;
    @FXML
    Label entertainmentCenterLabel;
    @FXML
    Spinner<Integer> entertainmentCenterSpinner;
    @FXML
    Label blackMarketLabel;
    @FXML
    Spinner<Integer> blackMarketSpinner;
    //military
    @FXML
    Label barracksLabel;
    @FXML
    Spinner<Integer> barracksSpinner;
    @FXML
    Label militaryFactoryLabel;
    @FXML
    Spinner<Integer> militaryFactorySpinner;
    @FXML
    Label airfieldLabel;
    @FXML
    Spinner<Integer> airfieldSpinner;
    @FXML
    Label shipyardLabel;
    @FXML
    Spinner<Integer> shipyardSpinner;
    @FXML
    Label defenseComplexLabel;
    @FXML
    Spinner<Integer> defenseComplexSpinner;
    @FXML
    Label gatesLabel;
    @FXML
    Spinner<Integer> gatesSpinner;
    @FXML
    Label engineeringComplexLabel;
    @FXML
    Spinner<Integer> engineeringComplexSpinner;
    @FXML
    Label oscdFabricLabel;
    @FXML
    Spinner<Integer> oscdFabricSpinner;
    //research
//    energySpinner
//    alloySpinner
//    scienceSpinner
//    ikeaSpinner
    @FXML
    Label energyLabel;
    @FXML
    Spinner<Integer> energySpinner;
    @FXML
    Label alloyLabel;
    @FXML
    Spinner<Integer> alloySpinner;
    @FXML
    Label scienceLabel;
    @FXML
    Spinner<Integer> scienceSpinner;
    @FXML
    Label ikeaLabel;
    @FXML
    Spinner<Integer> ikeaSpinner;
    @FXML
    Label defenseEngineeringLabel;
    @FXML
    Spinner<Integer> defenseEngineeringSpinner;
    @FXML
    Label hyperdriveLabel;
    @FXML
    Spinner<Integer> hyperdriveSpinner;
    @FXML
    Label nanotechnologyLabel;
    @FXML
    Spinner<Integer> nanotechnologySpinner;
    @FXML
    Label plasmoidConverterLabel;
    @FXML
    Spinner<Integer> plasmoidConverterSpinner;
    @FXML
    Label doomDayCalibrationLabel;
    @FXML
    Spinner<Integer> doomDayCalibrationSpinner;
    //achievements
    @FXML
    CheckBox lepreconKillerCheckBox;
    @FXML
    CheckBox pirateRaidCheckBox;
    @FXML
    CheckBox braveCapitanCheckBox;
    @FXML
    CheckBox headlessAdmiralCheckBox;
    @FXML
    CheckBox coldbloodedCalmlyCheckBox;
    @FXML
    CheckBox lightBringerCheckBox;
    @FXML
    CheckBox levTolstoyCheckBox;
    @FXML
    CheckBox satanMinisterCheckBox;
    //donate
    @FXML
    CheckBox crazyBonusCheckBox;
    //flat
    @FXML
    ComboBox<Room> roomComboBox;
    @FXML
    ComboBox<Tron> tronComboBox;
    @FXML
    ComboBox<Avatar> avatarComboBox;
    //damage modifier percents
    @FXML
    Slider humanDamageSlider;
    @FXML
    Slider reptileDamageSlider;
    @FXML
    CheckBox saveDamagePercents;
    //pro percent settings
    @FXML
    Pane percentBonusesPane;
    @FXML
    Spinner<Double> attackUnitCustomSpinner;
    @FXML
    Spinner<Double> healthUnitCustomSpinner;
    @FXML
    Spinner<Double> attackDefenseCustomSpinner;
    @FXML
    Spinner<Double> healthDefenseCustomSpinner;

    @FXML
    CheckBox isDefenseOfHome;

    @FXML
    CheckBox customReptileFleetMakingCheckBox;

    @FXML
    GridPane humanDefensePane;

    @FXML
    GridPane reptileFleetPane;

    @FXML
    Label versionNumberLabel;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        addSpaceUnitsToPane(NameToClassLinker.getInstance().getShipClasses(), humanShipsInputPane);
        addUpdateLvlsToPane(NameToClassLinker.getInstance().getShipClasses(), humanShipsUpdatesPane);

        addSpaceUnitsToPane(NameToClassLinker.getInstance().getDefenseClasses(), humanDefensePane);
        addSpaceUnitsToPane(NameToClassLinker.getInstance().getReptileClasses(), reptileFleetPane);

        fillDefaultComboBoxes();
        addValueFactoriesToSpinners();
        configureSpinners();

        versionNumberLabel.setText(Version.getVersion());

        isDefenseOfHome.selectedProperty().addListener(e -> setBlockVisibility(isDefenseOfHome.isSelected(), humanDefensePane));

        customReptileFleetMakingCheckBox.selectedProperty().addListener(e -> {
            setBlockVisibility(customReptileFleetMakingCheckBox.isSelected(), reptileFleetPane);
            clearInputs(NameToClassLinker.getInstance().getReptileClasses(), "Count");
        });

        alwaysOnTop.selectedProperty().addListener(observable -> {
            windowAlwaysOnTop = alwaysOnTop.isSelected();
            getStage().setAlwaysOnTop(windowAlwaysOnTop);
        });

    }

    private void addSpaceUnitsToPane(Collection<Class<? extends SpaceUnit>> units, GridPane gridPane) {
        int count = 0;
        double elementWidth = gridPane.getPrefWidth() / units.size();
        for (Class<? extends SpaceUnit> aClass : units) {
            Pane innerPane = (Pane) getShipInputPane(aClass);
            innerPane.setPrefWidth(elementWidth);
            innerPane.prefWidthProperty().bind(gridPane.widthProperty());
            gridPane.addColumn(count++, innerPane);
        }
    }

    private void addUpdateLvlsToPane(Collection<Class<? extends SpaceUnit>> units, GridPane gridPane) {
        int count = 0;
        double elementWidth = gridPane.getPrefWidth() / units.size();
        for (Class<? extends SpaceUnit> aClass : units) {
            Pane innerPane = (Pane) getShipUpdatePane(aClass);
            innerPane.setPrefWidth(elementWidth);
            innerPane.prefWidthProperty().bind(gridPane.widthProperty());
            gridPane.addColumn(count++, innerPane);
        }
    }

    @Override
    public void postInitialization() {
        setBlockVisibility(isDefenseOfHome.isSelected(), humanDefensePane);
        setBlockVisibility(isDefenseOfHome.isSelected(), reptileFleetPane);
        loadSavedProperties();
    }

    private void setBlockVisibility(boolean selected, Pane panelToShowOrHide) {
        panelToShowOrHide.setVisible(selected);
        panelToShowOrHide.setManaged(selected);
        double delta = panelToShowOrHide.getHeight();
        if (!selected) {
            delta = -delta;
        }
        TabPane corePane = (TabPane) getView();
        corePane.setPrefHeight(corePane.getHeight() + delta);
        getStage().sizeToScene();
    }

    @SuppressWarnings("unchecked")
    private void clearInputs(Collection<Class<? extends SpaceUnit>> elements, String suffix) {
        elements.forEach(aClass -> {
            Spinner<Integer> spinner = (Spinner<Integer>) getView().lookup("#" + aClass.getSimpleName() + suffix);
            spinner.getValueFactory().setValue(0);
        });
    }

    private Node getShipInputPane(Class<? extends SpaceUnit> spaceUnitClass) {
        BorderPane pane = new BorderPane();
        pane.setId(spaceUnitClass.getCanonicalName());

        Label shipName = new Label();
        shipName.setId(spaceUnitClass.getSimpleName() + "Name");
        shipName.setText(NameToClassLinker.getInstance().getNameForClass(spaceUnitClass));
        shipName.setPrefWidth(999.0);
        shipName.setAlignment(Pos.CENTER);
        shipName.setWrapText(true);

        Spinner spinner = new Spinner<Integer>();
        spinner = configureSpinnerWithFactory(spinner, getSpinnerIntegerFactory(Integer.MAX_VALUE));
        spinner.prefWidthProperty().bind(pane.widthProperty().subtract(10.0));
        spinner.setEditable(true);
        spinner.setId(spaceUnitClass.getSimpleName() + "Count");

        pane.setTop(shipName);
        pane.setCenter(spinner);

        return pane;
    }

    private Node getShipUpdatePane(Class<? extends SpaceUnit> spaceUnitClass) {
        BorderPane borderPane = new BorderPane();
        Spinner spinner = new Spinner<Integer>();
        spinner = configureSpinnerWithFactory(spinner, getSpinnerIntegerFactory(100));
        spinner.prefWidthProperty().bind(borderPane.widthProperty().subtract(10.0));
        spinner.setEditable(true);
        spinner.setId(spaceUnitClass.getSimpleName() + "UpdateLvl");

        borderPane.setCenter(spinner);

        return borderPane;
    }

    @SuppressWarnings("unchecked")
    private Spinner configureSpinnerWithFactory(Spinner spinner, SpinnerValueFactory factory) {
        spinner.setValueFactory(factory);

        spinner.focusedProperty().addListener((observable, oldValue, newValue) -> selectTextIfFocused(spinner));

        TextFormatter formatter = new TextFormatter(factory.getConverter(), factory.getValue());
        spinner.getEditor().setTextFormatter(formatter);
        // needed to force spinner to validate value on focus lost
        factory.valueProperty().bindBidirectional(formatter.valueProperty());
        spinner.getEditor().setOnKeyPressed(event -> {
            switch (event.getCode()) {
                case UP:
                    spinner.increment(1);
                    break;
                case DOWN:
                    spinner.decrement(1);
                    break;
            }
        });

        return spinner;
    }

    private SpinnerValueFactory getSpinnerIntegerFactory(int maxValue) {
        return new SpinnerValueFactory.IntegerSpinnerValueFactory(0, maxValue, 0);
    }

    private SpinnerValueFactory getSpinnerDoubleFactory() {
        return getSpinnerDoubleFactory(100.0);
    }

    private SpinnerValueFactory getSpinnerDoubleFactory(Double maxValue) {
        return new SpinnerValueFactory.DoubleSpinnerValueFactory(0, maxValue, 0, 0.1);
    }

    private void selectTextIfFocused(Spinner spinner) {
        Platform.runLater(() -> {
            if ((spinner.getEditor().isFocused() || spinner.isFocused()) && !spinner.getEditor().getText().isEmpty()) {
                spinner.getEditor().selectAll();
            }
            if ((!spinner.getEditor().isFocused() && !spinner.isFocused()) && spinner.getEditor().getText().isEmpty()) {
                spinner.getEditor().setText("0");
            }
        });
    }

    private void fillDefaultComboBoxes() {
        fillAttackComboBox();
        fillComplexityComboBox();
        fillFlatItemComboBox(roomComboBox, Room.values());
        fillFlatItemComboBox(tronComboBox, Tron.values());
        fillFlatItemComboBox(avatarComboBox, Avatar.values());
    }

    private void addValueFactoriesToSpinners() {
        configureSpinnerWithFactory(spacePortSpinner, getSpinnerIntegerFactory(100));
        configureSpinnerWithFactory(entertainmentCenterSpinner, getSpinnerIntegerFactory(100));
        configureSpinnerWithFactory(blackMarketSpinner, getSpinnerIntegerFactory(100));

        configureSpinnerWithFactory(barracksSpinner, getSpinnerIntegerFactory(100));
        configureSpinnerWithFactory(militaryFactorySpinner, getSpinnerIntegerFactory(100));
        configureSpinnerWithFactory(airfieldSpinner, getSpinnerIntegerFactory(100));
        configureSpinnerWithFactory(shipyardSpinner, getSpinnerIntegerFactory(100));
        configureSpinnerWithFactory(defenseComplexSpinner, getSpinnerIntegerFactory(100));
        configureSpinnerWithFactory(gatesSpinner, getSpinnerIntegerFactory(100));
        configureSpinnerWithFactory(engineeringComplexSpinner, getSpinnerIntegerFactory(100));
        configureSpinnerWithFactory(oscdFabricSpinner, getSpinnerIntegerFactory(100));

        configureSpinnerWithFactory(energySpinner, getSpinnerIntegerFactory(100));
        configureSpinnerWithFactory(alloySpinner, getSpinnerIntegerFactory(100));
        configureSpinnerWithFactory(scienceSpinner, getSpinnerIntegerFactory(100));
        configureSpinnerWithFactory(ikeaSpinner, getSpinnerIntegerFactory(100));
        configureSpinnerWithFactory(defenseEngineeringSpinner, getSpinnerIntegerFactory(100));
        configureSpinnerWithFactory(hyperdriveSpinner, getSpinnerIntegerFactory(100));
        configureSpinnerWithFactory(nanotechnologySpinner, getSpinnerIntegerFactory(100));
        configureSpinnerWithFactory(plasmoidConverterSpinner, getSpinnerIntegerFactory(100));
        configureSpinnerWithFactory(doomDayCalibrationSpinner, getSpinnerIntegerFactory(100));

        configureSpinnerWithFactory(attackUnitCustomSpinner, getSpinnerDoubleFactory());
        configureSpinnerWithFactory(healthUnitCustomSpinner, getSpinnerDoubleFactory());
        configureSpinnerWithFactory(attackDefenseCustomSpinner, getSpinnerDoubleFactory());
        configureSpinnerWithFactory(healthDefenseCustomSpinner, getSpinnerDoubleFactory());

    }

    private void configureSpinners() {
        DoubleBinding buildingW = ((VBox) spacePortSpinner.getParent().getParent()).widthProperty().divide(2).subtract(5);
        spacePortLabel.prefWidthProperty().bind(buildingW);
        spacePortSpinner.prefWidthProperty().bind(buildingW);
        entertainmentCenterLabel.prefWidthProperty().bind(buildingW);
        entertainmentCenterSpinner.prefWidthProperty().bind(buildingW);
        blackMarketLabel.prefWidthProperty().bind(buildingW);
        blackMarketSpinner.prefWidthProperty().bind(buildingW);

        DoubleBinding militaryW = ((VBox) barracksSpinner.getParent().getParent()).widthProperty().divide(2).subtract(5);
        barracksLabel.prefWidthProperty().bind(militaryW);
        barracksSpinner.prefWidthProperty().bind(militaryW);
        militaryFactoryLabel.prefWidthProperty().bind(militaryW);
        militaryFactorySpinner.prefWidthProperty().bind(militaryW);
        airfieldLabel.prefWidthProperty().bind(militaryW);
        airfieldSpinner.prefWidthProperty().bind(militaryW);
        shipyardLabel.prefWidthProperty().bind(militaryW);
        shipyardSpinner.prefWidthProperty().bind(militaryW);
        engineeringComplexLabel.prefWidthProperty().bind(militaryW);
        engineeringComplexSpinner.prefWidthProperty().bind(militaryW);
        gatesLabel.prefWidthProperty().bind(militaryW);
        gatesSpinner.prefWidthProperty().bind(militaryW);
        defenseComplexLabel.prefWidthProperty().bind(militaryW);
        defenseComplexSpinner.prefWidthProperty().bind(militaryW);
        oscdFabricLabel.prefWidthProperty().bind(militaryW);
        oscdFabricSpinner.prefWidthProperty().bind(militaryW);


        DoubleBinding researchW = ((VBox) energySpinner.getParent().getParent()).widthProperty().divide(2).subtract(5);
        energyLabel.prefWidthProperty().bind(researchW);
        energySpinner.prefWidthProperty().bind(researchW);
        alloyLabel.prefWidthProperty().bind(researchW);
        alloySpinner.prefWidthProperty().bind(researchW);
        scienceLabel.prefWidthProperty().bind(researchW);
        scienceSpinner.prefWidthProperty().bind(researchW);
        ikeaLabel.prefWidthProperty().bind(researchW);
        ikeaSpinner.prefWidthProperty().bind(researchW);
        defenseEngineeringLabel.prefWidthProperty().bind(researchW);
        defenseEngineeringSpinner.prefWidthProperty().bind(researchW);
        hyperdriveLabel.prefWidthProperty().bind(researchW);
        hyperdriveSpinner.prefWidthProperty().bind(researchW);
        doomDayCalibrationLabel.prefWidthProperty().bind(researchW);
        doomDayCalibrationSpinner.prefWidthProperty().bind(researchW);
        plasmoidConverterLabel.prefWidthProperty().bind(researchW);
        plasmoidConverterSpinner.prefWidthProperty().bind(researchW);
        nanotechnologySpinner.prefWidthProperty().bind(researchW);
    }

    private void fillAttackComboBox() {
        attackComboBox.getItems().addAll(AttackType.values());
        attackComboBox.getSelectionModel().selectFirst();
    }

    private void fillComplexityComboBox() {
        complexityComboBox.getItems().addAll(Complexity.values());
        complexityComboBox.getSelectionModel().selectFirst();
    }

    @SuppressWarnings("unchecked")
    private void fillFlatItemComboBox(ComboBox spinner, Object[] items) {
        spinner.getItems().clear();
        spinner.getItems().addAll(items);
        spinner.getSelectionModel().selectFirst();
    }

    @SuppressWarnings("unchecked")
    private BattleConfig saveSpinnersAndCheckBoxesValues() {
        BattleConfig battleConfig = new BattleConfig();
        battleConfig.setAttackType(attackComboBox.getValue());
        battleConfig.setComplexity(complexityComboBox.getValue());
        if (AttackType.IAMREADY == attackComboBox.getValue() || AttackType.PRISON == attackComboBox.getValue()){
            battleConfig.setComplexity(Complexity.ONE);
        }
        //donate
        battleConfig.setCrazyDonateBonus(crazyBonusCheckBox.isSelected());
        //achievements
        battleConfig.setSatanMinister(satanMinisterCheckBox.isSelected());
        battleConfig.setColdbloodedCalmly(coldbloodedCalmlyCheckBox.isSelected());
        battleConfig.setPirateRaid(pirateRaidCheckBox.isSelected());
        battleConfig.setBraveCaptain(braveCapitanCheckBox.isSelected());
        battleConfig.setHeadlessAdmiral(headlessAdmiralCheckBox.isSelected());
        battleConfig.setLepreconKiller(lepreconKillerCheckBox.isSelected());
        battleConfig.setLightBringer(lightBringerCheckBox.isSelected());
        battleConfig.setLevTolstoy(levTolstoyCheckBox.isSelected());
        //research
        battleConfig.setEnergyLvl(energySpinner.getValue());
        battleConfig.setAlloyLvl(alloySpinner.getValue());
        battleConfig.setScienceLvl(scienceSpinner.getValue());
        battleConfig.setIkeaLvl(ikeaSpinner.getValue());
        battleConfig.setDefenseEngineeringLvl(defenseEngineeringSpinner.getValue());
        battleConfig.setHyperdriveLvl(hyperdriveSpinner.getValue());
        battleConfig.setNanotechnologyLvl(nanotechnologySpinner.getValue());
        battleConfig.setPlasmoidConverterLvl(plasmoidConverterSpinner.getValue());
        battleConfig.setDoomDayCalibrationLvl(doomDayCalibrationSpinner.getValue());
        //building residential
        battleConfig.setSpacePortLvl(spacePortSpinner.getValue());
        battleConfig.setEntertainmentCenterLvl(entertainmentCenterSpinner.getValue());
        battleConfig.setBlackMarketLvl(blackMarketSpinner.getValue());
        //building military
        battleConfig.setBarracksLvl(barracksSpinner.getValue());
        battleConfig.setMilitaryFactoryLvl(militaryFactorySpinner.getValue());
        battleConfig.setAirfieldLvl(airfieldSpinner.getValue());
        battleConfig.setShipyyardLvl(shipyardSpinner.getValue());
        battleConfig.setDefenseComplexLvl(defenseComplexSpinner.getValue());
        battleConfig.setGatesLvl(gatesSpinner.getValue());
        battleConfig.setEngineeringComplexLvl(engineeringComplexSpinner.getValue());
        battleConfig.setOscdFabricLvl(oscdFabricSpinner.getValue());
        //room
        battleConfig.setBonusItems(Arrays.asList(
                roomComboBox.getValue(),
                tronComboBox.getValue(),
                avatarComboBox.getValue()
        ));
        //general damage
        battleConfig.setHumanDamagePercent(humanDamageSlider.getValue() / 100d);
        battleConfig.setReptileDamagePercent(reptileDamageSlider.getValue() / 100d);
        //custom
        battleConfig.setAttackUnitCustomPercent(attackUnitCustomSpinner.getValue() / 100d);
        battleConfig.setHealthUnitCustomPercent(healthUnitCustomSpinner.getValue() / 100d);
        battleConfig.setAttackDefenceCustomPercent(attackDefenseCustomSpinner.getValue() / 100d);
        battleConfig.setHealthDefenceCustomPercent(healthDefenseCustomSpinner.getValue() / 100d);

        Map<Class<? extends SpaceUnit>, Integer> updatesMap = new LinkedHashMap<>();
        NameToClassLinker.getInstance().getShipClasses().forEach(aClass -> {
            Spinner<Integer> spinner = (Spinner<Integer>) getView().lookup("#" + aClass.getSimpleName() + "UpdateLvl");
            updatesMap.put(aClass, spinner.getValue());
        });
        battleConfig.setUpdatesMap(updatesMap);

        return battleConfig;
    }

    @SuppressWarnings("unchecked")
    private Map<Class<? extends SpaceUnit>, UnitGroup> makeHumanFleet(BattleConfig battleConfig) {
        Map<Class<? extends SpaceUnit>, UnitGroup> humans = new LinkedHashMap<>();

        NameToClassLinker.getInstance().getShipClasses().forEach(aClass -> {
            Spinner<Integer> spinner = (Spinner<Integer>) getView().lookup("#" + aClass.getSimpleName() + "Count");
            if (spinner.getValue() > 0) {
                addShipsToMap(humans,
                        UnitGroup.create(SpaceUnitCreationHelper.getInstance().getInstanceOf(aClass, battleConfig), spinner.getValue()));
            }
        });

        if (isDefenseOfHome.isSelected()) {

            NameToClassLinker.getInstance().getDefenseClasses().forEach(aClass -> {
                Spinner<Integer> spinner = (Spinner<Integer>) getView().lookup("#" + aClass.getSimpleName() + "Count");
                if (spinner.getValue() > 0) {
                    addShipsToMap(humans,
                            UnitGroup.create(SpaceUnitCreationHelper.getInstance().getInstanceOf(aClass, battleConfig), spinner.getValue()));
                }
            });
        }

        return humans;
    }

    private void addShipsToMap(Map<Class<? extends SpaceUnit>, UnitGroup> fleet, UnitGroup unitGroup) {
        fleet.put(unitGroup.getUnitClass(), unitGroup);
    }

    private Map<Class<? extends SpaceUnit>, UnitGroup> makeReptileFleet(BattleConfig battleConfig) {
        Map<Class<? extends SpaceUnit>, UnitGroup> reptiles;
        if (!customReptileFleetMakingCheckBox.isSelected()) {
            reptiles = ReptileFleetProvider.getInstance().getFleetsByTypeAndLvl(battleConfig);
        } else {
            reptiles = makeCustomReptileFleet(battleConfig);
        }

        return reptiles;
    }

    @SuppressWarnings("unchecked")
    private Map<Class<? extends SpaceUnit>, UnitGroup> makeCustomReptileFleet(BattleConfig battleConfig) {
        Map<Class<? extends SpaceUnit>, UnitGroup> reptileFleet = new LinkedHashMap<>();

        NameToClassLinker.getInstance().getReptileClasses().forEach(aClass -> {
            Spinner<Integer> spinner = (Spinner<Integer>) getView().lookup("#" + aClass.getSimpleName() + "Count");
            if (spinner.getValue() > 0) {
                addShipsToMap(reptileFleet,
                        UnitGroup.create(SpaceUnitCreationHelper.getInstance().getInstanceOf(aClass, battleConfig), spinner.getValue()));
            }
        });
        return reptileFleet;
    }

    @FXML
    public void clearFleet() {
        clearInputs(NameToClassLinker.getInstance().getShipClasses(), "Count");
    }

    @FXML
    public void clearUpdates() {
        clearInputs(NameToClassLinker.getInstance().getShipClasses(), "UpdateLvl");
    }

    @FXML
    public void saveSettings() {
        SettingsProvider settingsProvider = SettingsProvider.getInstance();

        //ship updates
        NameToClassLinker.getInstance().getShipClasses().forEach(aClass -> {
            String id = aClass.getSimpleName() + "UpdateLvl";
            Spinner<Integer> spinner = (Spinner<Integer>) getView().lookup("#" + id);
            settingsProvider.addProperty(id, spinner.getValue().toString());
        });

        settingsProvider
                //specials
                .addProperty("alwaysOnTop", Boolean.toString(alwaysOnTop.isSelected()))
                .save();
    }

    @FXML
    public void saveGlobalSettings() {
        SettingsProvider.getInstance()
                //global bonuses
                .addProperty("crazyBonusCheckBox", Boolean.toString(crazyBonusCheckBox.isSelected()))
                //achievements
                .addProperty("lepreconKillerCheckBox", Boolean.toString(lepreconKillerCheckBox.isSelected()))
                .addProperty("pirateRaidCheckBox", Boolean.toString(pirateRaidCheckBox.isSelected()))
                .addProperty("braveCapitanCheckBox", Boolean.toString(braveCapitanCheckBox.isSelected()))
                .addProperty("headlessAdmiralCheckBox", Boolean.toString(headlessAdmiralCheckBox.isSelected()))
                .addProperty("coldbloodedCalmlyCheckBox", Boolean.toString(coldbloodedCalmlyCheckBox.isSelected()))
                .addProperty("lightBringerCheckBox", Boolean.toString(lightBringerCheckBox.isSelected()))
                .addProperty("levTolstoyCheckBox", Boolean.toString(levTolstoyCheckBox.isSelected()))
                .addProperty("satanMinisterCheckBox", Boolean.toString(satanMinisterCheckBox.isSelected()))
                // flat
                .addProperty("roomComboBox", (roomComboBox.getSelectionModel().getSelectedItem()).name())
                .addProperty("tronComboBox", (tronComboBox.getSelectionModel().getSelectedItem()).name())
                .addProperty("avatarComboBox", (avatarComboBox.getSelectionModel().getSelectedItem()).name())
                //research
                .addProperty("energySpinner", energySpinner.getValue().toString())
                .addProperty("alloySpinner", alloySpinner.getValue().toString())
                .addProperty("scienceSpinner", scienceSpinner.getValue().toString())
                .addProperty("ikeaSpinner", ikeaSpinner.getValue().toString())
                .addProperty("defenseEngineeringSpinner", defenseEngineeringSpinner.getValue().toString())
                .addProperty("hyperdriveSpinner", hyperdriveSpinner.getValue().toString())
                .addProperty("nanotechnologySpinner", nanotechnologySpinner.getValue().toString())
                .addProperty("plasmoidConverterSpinner", plasmoidConverterSpinner.getValue().toString())
                .addProperty("doomDayCalibrationSpinner", doomDayCalibrationSpinner.getValue().toString())
                //building residential
                .addProperty("spacePortSpinner", spacePortSpinner.getValue().toString())
                .addProperty("entertainmentCenterSpinner", entertainmentCenterSpinner.getValue().toString())
                .addProperty("blackMarketSpinner", blackMarketSpinner.getValue().toString())
                //building military
                .addProperty("barracksSpinner", barracksSpinner.getValue().toString())
                .addProperty("militaryFactorySpinner", militaryFactorySpinner.getValue().toString())
                .addProperty("airfieldSpinner", airfieldSpinner.getValue().toString())
                .addProperty("shipyardSpinner", shipyardSpinner.getValue().toString())
                .addProperty("defenseComplexSpinner", defenseComplexSpinner.getValue().toString())
                .addProperty("gatesSpinner", gatesSpinner.getValue().toString())
                .addProperty("engineeringComplexSpinner", engineeringComplexSpinner.getValue().toString())
                .addProperty("oscdFabricSpinner", oscdFabricSpinner.getValue().toString())
                // general
                .addProperty("saveDamagePercents", Boolean.toString(saveDamagePercents.isSelected()))
                .save();

        if (saveDamagePercents.isSelected()) {
            SettingsProvider.getInstance()
                    .addProperty("humanDamageSlider", Double.toString(humanDamageSlider.getValue()))
                    .addProperty("reptileDamageSlider", Double.toString(reptileDamageSlider.getValue()))
                    .save();
        }
    }

    private void loadSavedProperties() {
        SettingsProvider sp = SettingsProvider.getInstance();
        if (sp.propertiesExist()) {
            //ship updates
            NameToClassLinker.getInstance().getShipClasses().forEach(aClass -> {
                String id = aClass.getSimpleName() + "UpdateLvl";
                Spinner<Integer> spinner = (Spinner<Integer>) getView().lookup("#" + id);

                spinner.getValueFactory().setValue(Integer.parseInt(sp.getProperty(id, "0")));
                sp.addProperty(id, spinner.getValue().toString());
            });

            //global bonuses
            crazyBonusCheckBox.setSelected(Boolean.parseBoolean(sp.getProperty("crazyBonusCheckBox", "false")));
            saveDamagePercents.setSelected(Boolean.parseBoolean(sp.getProperty("saveDamagePercents", "false")));
            humanDamageSlider.setValue(Double.parseDouble(sp.getProperty("humanDamageSlider", "0.0")));
            reptileDamageSlider.setValue(Double.parseDouble(sp.getProperty("reptileDamageSlider", "100.0")));
            //achievements
            lepreconKillerCheckBox.setSelected(Boolean.parseBoolean(sp.getProperty("lepreconKillerCheckBox", "false")));
            pirateRaidCheckBox.setSelected(Boolean.parseBoolean(sp.getProperty("pirateRaidCheckBox", "false")));
            braveCapitanCheckBox.setSelected(Boolean.parseBoolean(sp.getProperty("braveCapitanCheckBox", "false")));
            headlessAdmiralCheckBox.setSelected(Boolean.parseBoolean(sp.getProperty("headlessAdmiralCheckBox", "false")));
            coldbloodedCalmlyCheckBox.setSelected(Boolean.parseBoolean(sp.getProperty("coldbloodedCalmlyCheckBox", "false")));
            lightBringerCheckBox.setSelected(Boolean.parseBoolean(sp.getProperty("lightBringerCheckBox", "false")));
            levTolstoyCheckBox.setSelected(Boolean.parseBoolean(sp.getProperty("levTolstoyCheckBox", "false")));
            satanMinisterCheckBox.setSelected(Boolean.parseBoolean(sp.getProperty("satanMinisterCheckBox", "false")));
            // flat
            roomComboBox.getSelectionModel().select(Room.valueOf(sp.getProperty("roomComboBox", Room.NO_BONUS.name())));
            tronComboBox.getSelectionModel().select(Tron.valueOf(sp.getProperty("tronComboBox", Tron.NO_BONUS.name())));
            avatarComboBox.getSelectionModel().select(Avatar.valueOf(sp.getProperty("avatarComboBox", Avatar.NO_BONUS.name())));
            //research
            energySpinner.getValueFactory().setValue(Integer.parseInt(sp.getProperty("energySpinner", "0")));
            alloySpinner.getValueFactory().setValue(Integer.parseInt(sp.getProperty("alloySpinner", "0")));
            scienceSpinner.getValueFactory().setValue(Integer.parseInt(sp.getProperty("scienceSpinner", "0")));
            ikeaSpinner.getValueFactory().setValue(Integer.parseInt(sp.getProperty("ikeaSpinner", "0")));
            defenseEngineeringSpinner.getValueFactory().setValue(Integer.parseInt(sp.getProperty("defenseEngineeringSpinner", "0")));
            hyperdriveSpinner.getValueFactory().setValue(Integer.parseInt(sp.getProperty("hyperdriveSpinner", "0")));
            nanotechnologySpinner.getValueFactory().setValue(Integer.parseInt(sp.getProperty("nanotechnologySpinner", "0")));
            plasmoidConverterSpinner.getValueFactory().setValue(Integer.parseInt(sp.getProperty("plasmoidConverterSpinner", "0")));
            doomDayCalibrationSpinner.getValueFactory().setValue(Integer.parseInt(sp.getProperty("doomDayCalibrationSpinner", "0")));
            //building residential
            spacePortSpinner.getValueFactory().setValue(Integer.parseInt(sp.getProperty("spacePortSpinner", "0")));
            entertainmentCenterSpinner.getValueFactory().setValue(Integer.parseInt(sp.getProperty("entertainmentCenterSpinner", "0")));
            blackMarketSpinner.getValueFactory().setValue(Integer.parseInt(sp.getProperty("blackMarketSpinner", "0")));
            //building military
            barracksSpinner.getValueFactory().setValue(Integer.parseInt(sp.getProperty("barracksSpinner", "0")));
            militaryFactorySpinner.getValueFactory().setValue(Integer.parseInt(sp.getProperty("militaryFactorySpinner", "0")));
            airfieldSpinner.getValueFactory().setValue(Integer.parseInt(sp.getProperty("airfieldSpinner", "0")));
            shipyardSpinner.getValueFactory().setValue(Integer.parseInt(sp.getProperty("shipyardSpinner", "0")));
            defenseComplexSpinner.getValueFactory().setValue(Integer.parseInt(sp.getProperty("defenseComplexSpinner", "0")));
            engineeringComplexSpinner.getValueFactory().setValue(Integer.parseInt(sp.getProperty("engineeringComplexSpinner", "0")));
            gatesSpinner.getValueFactory().setValue(Integer.parseInt(sp.getProperty("gatesSpinner", "0")));
            oscdFabricSpinner.getValueFactory().setValue(Integer.parseInt(sp.getProperty("oscdFabricSpinner", "0")));


            //specials
            alwaysOnTop.setSelected(Boolean.parseBoolean(sp.getProperty("alwaysOnTop", "false")));
        }
    }

    @FXML
    public void calculate() {
        createBattleResultDialog(BattleType.STANDART);
    }

    @FXML
    public void calculateLost() {
        createBattleResultDialog(BattleType.EXTERMINATION);
    }

    private void createBattleResultDialog(BattleType battleType) {
        BattleConfig battleConfig = saveSpinnersAndCheckBoxesValues();

        Map<Class<? extends SpaceUnit>, UnitGroup> reptiles = makeReptileFleet(battleConfig);

        Map<Class<? extends SpaceUnit>, UnitGroup> humans = makeHumanFleet(battleConfig);

        try (InputStream fxmlStream = MainFormController.class.getResourceAsStream("/view/BattleResult.fxml")) {
            FXMLLoader loader = new FXMLLoader();
            Parent view = loader.load(fxmlStream);
            Scene scene = new Scene(view);
            final Stage dialog = new Stage();
            dialog.initOwner(getStage());
            dialog.setScene(scene);
            dialog.initOwner(null);

            Controller controller = loader.getController();
            controller.setStage(dialog);
            controller.setView(view);
            controller.postInitialization();

            BattleCalculations battleCalculations = new BattleCalculations(humans, reptiles, battleConfig);
            battleCalculations.makeBattle(battleType);
            ((BattleResultController) controller).fillResults(battleCalculations);

            dialog.show();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @FXML
    public void iKnowWhatImDoingClick() {
        java.util.List<Node> paneComponents = new ArrayList<>();
        percentBonusesPane.getChildren().forEach(component -> paneComponents.addAll(((Pane) component).getChildren()));
        paneComponents.forEach(node -> node.setDisable(false));
    }

    @FXML
    public void infoButton() {
        try (InputStream fxmlStream = MainFormController.class.getResourceAsStream("/view/InfoDialog.fxml")) {
            FXMLLoader loader = new FXMLLoader();
            Parent view = loader.load(fxmlStream);
            Scene scene = new Scene(view);
            final Stage dialog = new Stage();
            dialog.initOwner(getStage());
            dialog.setScene(scene);
            dialog.initStyle(StageStyle.UNDECORATED);
            dialog.show();
            dialog.focusedProperty().addListener((observable, oldValue, newValue) -> {
                if (!newValue) {
                    dialog.close();
                }
            });

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
