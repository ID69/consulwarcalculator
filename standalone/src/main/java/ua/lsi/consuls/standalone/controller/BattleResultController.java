package ua.lsi.consuls.standalone.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import ua.lsi.consuls.core.battle.BattleCalculations;
import ua.lsi.consuls.core.battle.RoundResults;
import ua.lsi.consuls.core.model.ResourceContainer;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

/**
 * @author ID69
 */
public class BattleResultController extends AbstractController implements Initializable {

    @FXML
    ScrollPane containerScrollPane;
    @FXML
    Label humanStatus;
    @FXML
    Label reptileStatus;
    @FXML
    VBox roundSections;
    @FXML
    VBox wasGatheredPane;
    @FXML
    GridPane footerGridPane;

    @FXML
    Label metalLoses;
    @FXML
    Label crystalLoses;
    @FXML
    Label humanLoses;
    @FXML
    Label timeLoses;
    @FXML
    Label metalProfit;
    @FXML
    Label crystalProfit;
    @FXML
    Label honorProfit;
    @FXML
    Label metalReward;
    @FXML
    Label crystalReward;
    @FXML
    Label honorReward;
    @FXML
    Label metalRepair;
    @FXML
    Label crystalRepair;
    @FXML
    Label humanRepair;
    @FXML
    Button okButton;

    @FXML
    public void onOk() {
        getStage().close();
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
    }

    @Override
    public void postInitialization() {
        getStage().setAlwaysOnTop(MainFormController.windowAlwaysOnTop);
    }

    private void setLoses(ResourceContainer container) {
        setTextAndTooltip(metalLoses, container.getMetal().longValue());
        setTextAndTooltip(crystalLoses, container.getCrystal().longValue());
        setTextAndTooltip(humanLoses, container.getHuman());
        setTextAndTooltip(timeLoses, getFormatedTime(container.getTime()));
    }

    private void setProfit(ResourceContainer container) {
        setTextAndTooltip(metalProfit, container.getMetal().longValue());
        setTextAndTooltip(crystalProfit, container.getCrystal().longValue());
        setTextAndTooltip(honorProfit, container.getHonor().longValue());
    }

    private void setReward(ResourceContainer container) {
        setTextAndTooltip(metalReward, container.getMetal().longValue());
        setTextAndTooltip(crystalReward, container.getCrystal().longValue());
        setTextAndTooltip(honorReward, Math.round(container.getHonor()));
    }

    private void setRepair(ResourceContainer container) {
        setTextAndTooltip(metalRepair, container.getMetal().longValue());
        setTextAndTooltip(crystalRepair, container.getCrystal().longValue());
        setTextAndTooltip(humanRepair, container.getHuman());
    }

    private void setTextAndTooltip(Label label, Long count) {
        setTextAndTooltip(label, count, count.toString());
    }

    private void setTextAndTooltip(Label label, String text) {
        label.setText(text);
        label.setTooltip(new Tooltip(text));
    }

    private void setTextAndTooltip(Label label, Long count, String text) {
        label.setText(displayNumberWithSuffix(count));
        label.setTooltip(new Tooltip(text));
    }


    private String displayNumberWithSuffix(Long count) {
        boolean sign = false;
        if (count < 0) {
            sign = true;
            count = -count;
        }
        if (count < 100000) {
            return (sign ? "-" : "") + count;
        }
        int exp = (int) (Math.log(count) / Math.log(1000));
        return (sign ? "-" : "") + String.format("%4.2f%c",
                count / Math.pow(1000, exp),
                "kMGTPE".charAt(exp - 1));
    }

    private String getFormatedTime(Integer seconds) {
        long days = TimeUnit.SECONDS.toDays(seconds);
        long hours = TimeUnit.SECONDS.toHours(seconds);
        long minutes = TimeUnit.SECONDS.toMinutes(seconds);
        long sec = TimeUnit.SECONDS.toSeconds(seconds);
        //1d1h1m1s
        return String.format("%02dd%02dh%02dm%02ds",
                days,
                hours - TimeUnit.DAYS.toHours(days),
                minutes - TimeUnit.HOURS.toMinutes(hours),
                sec - TimeUnit.MINUTES.toSeconds(minutes));
    }

    public void fillResults(BattleCalculations battleCalculations) {
        humanStatus.setText(battleCalculations.getHumanStatus());
        reptileStatus.setText(battleCalculations.getReptileStatus());

        battleCalculations.getRounds().forEach(this::fillRound);

        setLoses(battleCalculations.getLoses());
        setRepair(battleCalculations.getRepair());
        setProfit(battleCalculations.getProfit());
        setReward(battleCalculations.getReward());
    }

    private void fillRound(RoundResults round) {
        RoundSectionController roundPane = addRound();
        roundPane.createColumns();
        roundPane.fillRound(round);
    }

    private RoundSectionController addRound() {
        try (InputStream fxmlStream = MainFormController.class.getResourceAsStream("/view/RoundSection.fxml")) {
            FXMLLoader loader = new FXMLLoader();
            Parent roundSection = loader.load(fxmlStream);

            RoundSectionController roundSectionController = loader.getController();
            roundSectionController.setView(roundSection);
            roundSectionController.postInitialization();
            roundSections.getChildren().add(roundSection);

            return roundSectionController;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
