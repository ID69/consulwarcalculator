package ua.lsi.consuls.standalone.controller;

import javafx.scene.Node;
import javafx.stage.Stage;

/**
 * @author ID69
 */
public interface Controller {
    Node getView();

    void setView(Node view);

    void setStage(Stage primaryStage);

    Stage getStage();

    void postInitialization();
}
